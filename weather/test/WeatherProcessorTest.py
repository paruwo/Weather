import unittest

from weather.protocol.csv.Air import Air


class WeatherProcessorTest(unittest.TestCase):
    def test_fix_Errors(self):
        wp = Air()
        self.assertEqual("0", wp.fix_data_errors("0"))
        self.assertEqual("1", wp.fix_data_errors("1"))
        self.assertEqual("0", wp.fix_data_errors("-999"))


"""
    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)
"""

if __name__ == "__main__":
    unittest.main()
