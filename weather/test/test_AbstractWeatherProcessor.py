from datetime import datetime

from weather.abstract_weather_processor import AbstractWeatherProcessor
from weather.protocol.http.air import Air


def test__to_pct_string():
    for i in range(0, 8):
        assert AbstractWeatherProcessor.to_pct_string(str(i)) == str(i / 8)

    assert AbstractWeatherProcessor.to_pct_string("-999") == "0"


def test_fix_data_errors():
    assert AbstractWeatherProcessor.fix_data_errors("-999") == "0"
    assert AbstractWeatherProcessor.fix_data_errors("-998") == "-998"
    assert AbstractWeatherProcessor.fix_data_errors("-1000") == "-1000"
    assert AbstractWeatherProcessor.fix_data_errors("0") == "0"
    assert AbstractWeatherProcessor.fix_data_errors("1.2345") == "1.2345"


def test_get_ts():
    # 2018-09-23 11:50 UTC = 1537703400 = 2018-09-23 13:50 +02:00
    # (at DWD localtime +02:00  = 1537710600)

    # Europe/Berlin
    # +02:00 starting at yyyy-03-31
    # +01:00 starting at yyyy-10-27

    ts = AbstractWeatherProcessor.str_to_ts("201809231150")
    assert "1537703400" == ts


def test_make_full():
    # test with http class (no further dependencies)
    wp = Air().make_full()
    assert wp._full_delta == "full"


def test_make_delta():
    # test with http class (no further dependencies)
    wp = Air().make_delta()
    assert wp._full_delta == "delta"


def test_make_hist():
    # test with http class (no further dependencies)
    wp = Air().make_hist()
    assert wp._full_delta == "hist"


def test_filter_hours():
    assert AbstractWeatherProcessor.filter_hours("asdasdprodukt_n_stunde_asdasd")
    assert AbstractWeatherProcessor.filter_hours("asdasdprodukt_vv_stunde_asdasd")
    assert AbstractWeatherProcessor.filter_hours("asdasd") is False


def test_dates_from_file_name():
    from_dt, to_dt = AbstractWeatherProcessor.dates_from_file_name("produkt_zehn_min_rr_20100101_20181231_05111.txt")
    assert from_dt == datetime.strptime("20100101", "%Y%m%d")
    assert to_dt == datetime.strptime("20181231", "%Y%m%d")


def test_filter_ten_minutes():
    # delta < 10d, full <=549d, hist > 549d
    file_1d = "produkt_zehn_min_rr_20181230_20181231_05111.txt"
    file_9d = "produkt_zehn_now_rr_20190601_20190609_05111.txt"
    file_10d = "produkt_zehn_now_rr_20190601_20190620_05111.txt"
    file_11d = "produkt_zehn_now_rr_20190601_20190621_05111.txt"
    file_549d = "produkt_zehn_min_rr_20171226_20190628_05111.txt"
    file_550d = "produkt_zehn_min_rr_20171226_20190629_05111.txt"

    assert AbstractWeatherProcessor.filter_ten_minutes(file_1d, "delta")
    assert AbstractWeatherProcessor.filter_ten_minutes(file_9d, "delta")
    assert AbstractWeatherProcessor.filter_ten_minutes(file_10d, "delta") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_11d, "delta") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_549d, "delta") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_550d, "delta") is False

    assert AbstractWeatherProcessor.filter_ten_minutes(file_1d, "full") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_9d, "full") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_10d, "full")
    assert AbstractWeatherProcessor.filter_ten_minutes(file_11d, "full")
    assert AbstractWeatherProcessor.filter_ten_minutes(file_549d, "full")
    assert AbstractWeatherProcessor.filter_ten_minutes(file_550d, "full") is False

    assert AbstractWeatherProcessor.filter_ten_minutes(file_1d, "hist") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_9d, "hist") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_10d, "hist") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_11d, "hist") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_549d, "hist") is False
    assert AbstractWeatherProcessor.filter_ten_minutes(file_550d, "hist")
