from abc import ABC
from datetime import datetime

from weather.protocol.database.weather_processor import WeatherProcessor
from weather.util.config import Config


class Air(WeatherProcessor, ABC):
    """Implement PostgreSQL data processing for air data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("air")
        self.table_name = "air"
        self.insert = Config().db_insert_air

    def parse_data(self, reader, station_id: int) -> None:
        """Read air-related data from observation file."""
        for record in reader:
            # skip header
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            timestamp = datetime.strptime(record[1].strip(), "%Y%m%d%H%M")
            # all of type float
            pressure = float(WeatherProcessor.fix_data_errors(record[3].strip()))
            temperature = round(float(WeatherProcessor.fix_data_errors(record[4].strip())), 1)
            humidity = float(WeatherProcessor.fix_data_errors(record[6].strip()))

            self.post_data_air(
                source=self._type,
                humidity=humidity,
                temperature=temperature,
                pressure=pressure,
                station_id=station_id,
                timestamp=timestamp,
            )

            self.print_tps(check_every=100)
        self._flush_data()

    # TODO implement
    def delete_forecast_points(self, forecast_type, forecast_name=None) -> None:
        """Delete air forecast data."""

    # TODO implement
    def insert_forecast_data(self) -> None:
        """Ingest air forecast data."""
