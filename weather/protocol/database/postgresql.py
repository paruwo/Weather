import logging
from pathlib import Path

from psycopg2 import connect
from psycopg2._psycopg import connection
from psycopg2.extensions import ISOLATION_LEVEL_READ_COMMITTED

from weather.util.config import Config
from weather.util.logger import Logger


class Postgresql:
    """A wrapper for PostgreSQL access."""

    db_objects = {
        "tables": ["air", "rain", "agg_air"],
        "procedures": ["load_agg_air"],
    }
    # TODO grafana: weather dashboard for monthly data only
    # TODO grafana: station name lookup
    # TODO grafana: refine and save dashboards to git with small source/database descriptions

    db_exists = {
        "tables": "select count(*) from pg_catalog.pg_tables where tablename = %s",
        "procedures": "select count(*) from pg_catalog.pg_proc where proname = %s",
    }

    log: logging.Logger
    conn: connection
    db_resources: Path
    missing_tables: list
    missing_procs: list

    def __init__(self):
        """Create an instance and connect to database."""
        self.log = Logger()

        # TODO connection pooling, see https://pynative.com/psycopg2-python-postgresql-connection-pooling/
        self.conn = connect(
            host=Config().db_host,
            dbname=Config().db_database,
            user=Config().db_user,
            password=Config().db_password,
            port=Config().db_port,
            sslmode=Config().ssl_mode,
        )
        self.conn.set_isolation_level(ISOLATION_LEVEL_READ_COMMITTED)
        self.db_resources = Config().project_root.parent / "database" / "postgresql"

        self.missing_tables = self.__check_object_exists("tables")
        self.missing_procs = self.__check_object_exists("procedures")
        self.__create_missing_objects()

    def __check_object_exists(self, object_type) -> list:
        missing = []
        cur = self.conn.cursor()
        check_query = self.db_exists[object_type]
        for obj in self.db_objects[object_type]:
            self.log.debug(check_query + "'" + obj + "'")
            cur.execute(check_query, (obj,))
            row = cur.fetchall()
            if row[0][0] != 1:
                missing.append(obj)
                self.log.warning(f"{object_type} {obj} does not exist")
        if len(missing) == 0:
            self.log.info(f"database {object_type} ok")
        return missing

    def __create_object(self, object_list) -> None:
        for item in object_list:
            database_file = item + ".sql"
            self.log.info(f'creating "{item}" from {self.db_resources / database_file}')
            with Path(self.db_resources / database_file).open("r", encoding="UTF-8") as sql_file:
                cur = self.conn.cursor()
                cur.execute(sql_file.read())
                self.conn.commit()

    def __create_missing_objects(self) -> None:
        self.__create_object(self.missing_tables)
        self.__create_object(self.missing_procs)
