import os
import time
from abc import abstractmethod
from datetime import datetime, timedelta
from typing import List, Optional

from psycopg2._psycopg import connection, cursor
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.extras import execute_values

from weather.abstract_weather_processor import AbstractWeatherProcessor
from weather.protocol.database.postgresql import Postgresql
from weather.util.config import Config


class WeatherProcessor(AbstractWeatherProcessor):
    """A concrete weather processor for PostgreSQL databases."""

    max_messages: int
    weather_payload: List
    conn: connection
    cur: cursor
    # following variables are set in subclasses:
    insert = None
    table_name: str

    def __init__(self, weather_type: str):
        """Initiate instance and open connection."""
        AbstractWeatherProcessor.__init__(self, weather_type)

        postgres = Postgresql()
        self.conn = postgres.conn

        self.cur = self.conn.cursor()
        self.max_messages = Config().db_batch_size
        self.weather_payload = []
        self.log.debug("database connection opened")

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Close existing connections before exit.

        :param exc_type:
        :param exc_val:
        :param exc_tb:
        """
        self.conn.rollback()
        self.conn.close()
        self.log.debug("database connection closed")

    def validate_pg_version(self) -> None:
        """Check that postgresql has at least version 13."""
        version = int(self.conn.server_version / 10000)
        if version >= 13:
            self.log.info(f"database version {version} is ok")
        else:
            self.log.error(f"database version {version} is unsupported")

    def __post_data(self) -> None:
        """
        Issue bulk-insert into database by leveraging execute_values().

        Afterwards the payload buffer is cleared. We should reach more than 30k TPS on a
        plain vanilla database with 2 vCPUs and 2 GB RAM without tuning under no load.
        """
        self.log.debug("execute bulk operation")

        execute_values(
            cur=self.cur,
            sql=self.insert,
            argslist=self.weather_payload,
            page_size=Config().db_batch_size,
        )
        self.conn.commit()
        self.weather_payload = []

    def post_data_air(
        self,
        source: Optional[str],
        temperature: float,
        humidity: float,
        pressure: float,
        station_id: int,
        timestamp,
    ) -> None:
        """
        Consume air payload until message threshold is hit, then flush.

        When this happens, collected data is bulk-inserted into database.
        :param source: weather type like 'air'
        :param temperature: in degree celsius
        :param humidity:
        :param pressure: in hPa, TODO open point is whether to use sea level or location level
        :param station_id: forecast station_id
        :param timestamp: timestamp
        """
        if source is not None:
            self.weather_payload.append((station_id, timestamp, temperature, humidity, pressure))

        if len(self.weather_payload) >= self.max_messages or source is None:
            self.__post_data()

    def post_data_rain(
        self,
        source: Optional[str],
        duration: int,
        height: float,
        rain_flag: bool,
        station_id: int,
        timestamp,
    ) -> None:
        """Consume rain payload until message threshold is hit, then flush."""
        if source is not None:
            self.weather_payload.append((station_id, timestamp, duration, height, rain_flag))

        if len(self.weather_payload) >= self.max_messages or source is None:
            self.__post_data()

    def _flush_data(self) -> None:
        """
        Ensure that all data is committed in database.

        To be called when there is no more payload to ingest. This one flushes the remaining
        data from buffer into database, finally.
        """
        self.log.debug("flushing remaining data from cache to target")
        if self._type == "air":
            self.post_data_air(
                source=None,
                temperature=0,
                humidity=0,
                pressure=0,
                station_id=0,
                timestamp=None,
            )
        elif self._type == "rain":
            self.post_data_rain(
                source=None,
                duration=0,
                height=0,
                rain_flag=False,
                station_id=0,
                timestamp=None,
            )
        self.conn.commit()
        self.log.debug("done")

    def __truncate_data(self, table_name: str = "") -> None:
        """
        Issue a 'truncate table' on whatever weather type is set.

        Table name to truncate is the same as weather type.
        """
        if table_name == "":
            table_name = self.table_name

        self.log.debug(f'truncating postgresql target "{table_name}"')
        self.conn.cursor().execute(query=f"TRUNCATE TABLE {table_name}")
        self.conn.commit()
        self.log.debug("done")

    def _cluster(self, table_name: str):
        self.log.info(f"re-clustering {table_name}")
        old_isolation_level = self.conn.isolation_level
        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.conn.cursor().execute(query=Config().db_cluster_table.format(table_name=table_name))
        self.conn.set_isolation_level(old_isolation_level)

    def _analyze(self, table_name: str):
        self.log.info(f"analyzing {table_name}")
        old_isolation_level = self.conn.isolation_level
        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.conn.cursor().execute(query=Config().db_analyze_table.format(table_name=table_name))
        self.conn.set_isolation_level(old_isolation_level)

    def _analyze_and_aggregate(self) -> None:
        """
        Execute a 'vacuum analyze' on the weather type table.

        Afterwards the access materialized view is refreshed and analyzed to gain optimal performance.
        """
        old_isolation_level = self.conn.isolation_level
        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)

        # original table
        self.log.info(f"analyzing {self.table_name}")
        self.conn.cursor().execute(query=Config().db_analyze_table.format(table_name=self.table_name))

        # aggregate
        if self._type == "air":
            self.log.info("providing aggregate table with data")
            if self._full_delta == "hist":
                threshold = ""
            else:
                if self._full_delta == "delta":
                    threshold = datetime.now() - timedelta(days=Config().dwd_delta_timerange)
                else:
                    # full
                    threshold = datetime.now() - timedelta(days=Config().dwd_full_timerange)
                threshold = f"timestamp '{threshold.strftime('%Y-%m-%d %H:%M:%S')}'"

            self.log.debug(f"calling load_agg_air({threshold})")
            self.conn.cursor().execute(query=f"call load_agg_air({threshold})")
            self.conn.commit()
            # no cluster atm.
            # self._cluster(table_name="agg_air")
            self._analyze(table_name="agg_air")

        self.conn.set_isolation_level(old_isolation_level)

    def _delete(self, threshold: datetime, comparator: str) -> None:
        if comparator not in (">", "<"):
            raise ValueError("invalid comparator")

        self.log.info(f"cleansing database for data {'earlier' if comparator == '<' else 'later'} than {threshold}")

        # determine deletion border
        table_name = self._type
        border = f"timestamp '{threshold.strftime('%Y-%m-%d %H:%M:%S')}'"
        query = Config().db_housekeeping_partitions_sql.format(table_name=table_name, comparator=comparator, border=border)
        self.log.debug(f"db query is {query}")

        # (1) truncate all matching partitions where possible
        self.cur.execute(query)
        partitions_to_clear = self.cur.fetchall()
        for partition in partitions_to_clear:
            self.log.info(f"    truncating and re-analyzing {partition[0]}")
            self.__truncate_data(partition[0])
            self._analyze(table_name=partition[0])

        # (2) delete the remaining rest
        self.log.info("    deleting remaining data")
        query = f"DELETE FROM {table_name} WHERE effective_at {comparator} :border"
        self.conn.cursor().execute(f"DELETE FROM {table_name} WHERE effective_at {comparator} %s", (border,))
        self.conn.commit()
        self.log.info("cleansing done.")

    def compile_data(self) -> None:
        """Load payload file, parse and process it, then do a cleanup."""
        # We generally ignore duplicates, only on demand, we truncate tables
        if Config().db_truncate:
            self.__truncate_data()

        self._last_start = int(round(time.time() * 1000))
        self._last_lines = 0

        files = []
        file_path = Config().project_root.parent / "data" / self._type

        self.log.meta_create_action("upload")
        self.log.info(f"importing {self._type} into postgresql")

        # for data_file in file_path.glob('**/*.txt'):
        # gives "TypeError: argument of type 'WindowsPath' is not iterable"
        for data_file in os.listdir(file_path):
            files.append(data_file)

        # ignore return type
        self._compile_data(files=files, file_path=file_path, parse_data=self.parse_data)
        self._analyze_and_aggregate()
        self.log.info(f"imported {self._lines} lines")

        self.log.meta_end_action("upload")
        self.log.meta_set_num_records(self._lines)
        self.log.meta_end()

        self.cleanup_old_files()

    @abstractmethod
    def parse_data(self, reader, station_id: int) -> None:
        """Open for concrete implementation based on weather type."""

    def run(self) -> None:
        """Fetch data and metadata, process, load, run housekeeping and call forecast loading."""
        self.fetch_data()
        self.compile_metadata()
        self.compile_data()
        self.housekeeping()

        # not implemented for database
        if Config().load_forecast:
            self.insert_forecast_data()
