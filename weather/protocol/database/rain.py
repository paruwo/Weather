from abc import ABC
from datetime import datetime

from weather.protocol.database.weather_processor import WeatherProcessor
from weather.util.config import Config


class Rain(WeatherProcessor, ABC):
    """Implement PostgreSQL data processing for rain data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("rain")
        self.table_name = "rain"
        self.insert = Config().db_insert_rain

    def parse_data(self, reader, station_id: int) -> None:
        """Read rain data from observation file."""
        for record in reader:
            # skip header
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            timestamp = datetime.strptime(record[1].strip(), "%Y%m%d%H%M")
            # all of type float
            duration = int(WeatherProcessor.fix_data_errors(record[3].strip()))
            height = round(float(WeatherProcessor.fix_data_errors(record[4].strip())), 2)
            rain_flag = WeatherProcessor.fix_data_errors(record[5].strip())

            # very old data might by corrupt
            if rain_flag not in ("0", "1"):
                rain_flag = False
            else:
                rain_flag = bool(rain_flag)

            self.post_data_rain(
                source=self._type,
                duration=duration,
                height=height,
                rain_flag=rain_flag,
                station_id=station_id,
                timestamp=timestamp,
            )

            self.print_tps(check_every=100)
        self._flush_data()

    # TODO implement
    def delete_forecast_points(self, forecast_type, forecast_name=None) -> None:
        """Delete rain forecast data."""

    # TODO implement
    def insert_forecast_data(self) -> None:
        """Ingest air forecast data."""
