import time
from datetime import datetime
from typing import Dict, List
from xml.dom.minidom import Document, parse

from weather.util.config import Config


class KmlReader:
    """
    Implement a reader facility for KML files: https://opendata.dwd.de/weather/lib/MetElementDefinition.xml.

    https://www.dwd.de/DE/leistungen/met_verfahren_mosmix/mosmix_stationskatalog.cfg?view=nasPublication&nn=16102
    The known KML files are small enough to be processed with minimal effort in RAM using minidom library.
    """

    doml: Document
    values: Dict
    time_steps: List

    def __init__(self, kml_file):
        """Initiate the instance."""
        self.dom1 = parse(kml_file)
        self.time_steps = self.get_time_steps()
        self.values = {}

    def get_time_steps(self) -> list:
        """
        Read the timestamps forecast data is available for.

        :return: list of timestamps
        """
        time_steps = []
        for node_list in self.dom1.getElementsByTagName("dwd:TimeStep"):
            time_steps.append(KmlReader.reformat_timestamp(node_list.firstChild.nodeValue))
        return time_steps

    @staticmethod
    def reformat_timestamp(ts_val) -> str:
        """
        Take the weather service forecast timestamps and process.

        Remove trailing Z (for Zulu) and append 3 zeros ('000') to indicate milliseconds. Improves parsing.
        """
        timestamp = datetime.strptime(ts_val[:-1] + "000", "%Y-%m-%dT%H:%M:%S.%f")
        return str(int(time.mktime(timestamp.utctimetuple())))

    def get_air_temperature(self, raw_data) -> list:
        """
        Enrich forecast data with air temperature.

        :param raw_data: temperature in degree Fahrenheit as string
        :return: forecast data enriched with degree Celsius temperature
        """
        # convert from Kelvin to Celsius
        temperature_forecast = [round(float(val) - 273.15, 2) for val in raw_data]
        # merge timestamps and temperatures
        return list(zip(self.time_steps, temperature_forecast))

    def get_solar(self, raw_data) -> list:
        """Get forecasted time for duration of sun shine."""
        solar_forecast = [int(float(val)) for val in raw_data]
        return list(zip(self.time_steps, solar_forecast))

    def get_rain_height(self, raw_data) -> list:
        """Get amount of rain in mm for forecast."""
        rain_forecast = [round(float(0 if val == "-" else val), 2) for val in raw_data]
        return list(zip(self.time_steps, rain_forecast))

    def get_air_pressure(self, raw_data) -> list:
        """Get air pressure forecast with respect to level 515m for now only."""
        # reduce pressure from sea level to 515m - FIXME only valid for Munich
        pressure_forecast = [round(0.9434 * float(val) / 100, 1) for val in raw_data]
        return list(zip(self.time_steps, pressure_forecast))

    def get_wind_direction(self, raw_data) -> list:
        """Get wind direction forecast."""
        direction_forecast = [int(float(val)) for val in raw_data]
        return list(zip(self.time_steps, direction_forecast))

    def get_wind_speed(self, raw_data) -> list:
        """Get wind speed forecast."""
        speed_forecast = [float(val) for val in raw_data]
        return list(zip(self.time_steps, speed_forecast))

    def get_forecast_values(self, weather_type=None) -> dict:
        """
        Parse forecast data for known types air, rain, solar and wind.

        :param weather_type: None - then all is computed, or one concrete type like 'air'
        :return: parsed forecast values as dictionary
        """
        if weather_type is None:
            weather_type = Config().weather_types

        # for detailed information: https://opendata.dwd.de/weather/lib/MetElementDefinition.xml
        for node in self.dom1.getElementsByTagName("dwd:Forecast"):
            for _, value in node.attributes.items():
                # if value in ('TTT', 'SunD1'):
                raw_data = node.getElementsByTagName("dwd:value")[0].firstChild.data.split()

                if value == "TTT" and "air" in weather_type:
                    # in °Celsius
                    self.values["temperature"] = self.get_air_temperature(raw_data)
                elif value == "PPPP" and "air" in weather_type:
                    # in hPa (from Pa)
                    self.values["pressure"] = self.get_air_pressure(raw_data)
                elif value == "SunD1" and "solar" in weather_type:
                    # in seconds/hour
                    self.values["solar"] = self.get_solar(raw_data)
                elif value == "RR1c" and "rain" in weather_type:
                    # in l/m²
                    self.values["rain"] = self.get_rain_height(raw_data)
                elif value == "DD" and "wind" in weather_type:
                    self.values["direction"] = self.get_wind_direction(raw_data)
                elif value == "FF" and "wind" in weather_type:
                    self.values["speed"] = self.get_wind_speed(raw_data)

        return self.values
