from weather.protocol.http.weather_processor import WeatherProcessor


class Air(WeatherProcessor):
    """Concrete processor for AIR data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("air")

    def parse_data(self, reader, station_id: int):
        """Read air-related data from observation file."""
        for record in reader:
            # skip header
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            timestamp = WeatherProcessor.str_to_ts(record[1].strip())
            # all of type float
            pressure = WeatherProcessor.fix_data_errors(record[3].strip())
            temperature = WeatherProcessor.fix_data_errors(record[4].strip())
            humidity = WeatherProcessor.fix_data_errors(record[6].strip())

            self.post_data(
                source=self._type,
                component="pressure",
                station_id=station_id,
                value=pressure,
                epoch=timestamp,
            )
            self.post_data(
                source=self._type,
                component="temperature",
                station_id=station_id,
                value=temperature,
                epoch=timestamp,
            )
            self.post_data(
                source=self._type,
                component="humidity",
                station_id=station_id,
                value=humidity,
                epoch=timestamp,
            )

            self.print_tps()

        self.log.debug(WeatherProcessor.fix_data_errors.cache_info())
        self.flush_data()
