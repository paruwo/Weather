from weather.protocol.http.weather_processor import WeatherProcessor


class AirMap(WeatherProcessor):
    """Concrete processor for SPATIAL AIR data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("air-map")

    def parse_data(self, reader, station_id):
        """Read air map data from observation file."""
        for record in reader:
            # skip header
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            timestamp = WeatherProcessor.str_to_ts(record[1].strip())
            temperature = WeatherProcessor.fix_data_errors(record[4].strip())
            geohash = self.stations[str(station_id)]["geohash"]
            location = self.stations[str(station_id)]["name"]

            self.post_map_air_data(
                source=self._type + "map",
                component="temperature",
                geohash=geohash,
                location=location,
                value=temperature,
                epoch=timestamp,
            )

            self.print_tps(check_every=10)

        self.log.debug(WeatherProcessor.fix_data_errors.cache_info())
        self.flush_data()
