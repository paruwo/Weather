"""The general weather processor, to be extended by special data import implementations."""

import gzip
import json
import os
import sys
import time
import zipfile
from abc import abstractmethod
from datetime import datetime, timedelta
from pathlib import Path
from typing import List, Optional, Tuple

import requests
import urllib3
from weather.abstract_weather_processor import AbstractWeatherProcessor
from weather.protocol.forecast.kml_reader import KmlReader
from weather.util.config import Config
from weather.util.config_reader import CfgReader
from weather.util.https_downloader import HttpsDownloader
from weather.util.logger import Logger


class WeatherProcessor(AbstractWeatherProcessor):
    """Weather processor for all http-based targets, currently influxdb."""

    url: str
    weather_payload: List
    map_air_payload: List
    """https://docs.influxdata.com/influxdb/v1.7/guides/writing_data/#writing-multiple-points"""
    max_messages: int
    compression: bool
    transmission_time: timedelta

    def __init__(self, weather_type: str):
        """Create a weather processor instance."""
        AbstractWeatherProcessor.__init__(self, weather_type)
        self.log.meta_set_upload_type("http")
        self.url = "".join([Config().influxdb_endpoint, "/write?db=", Config().influxdb_database_name])
        self.max_messages = int(Config().http_batch_size)
        self.compression = Config().http_compression
        self.weather_payload = []
        self.transmission_time = timedelta()
        self.log.info(f"target is {Config().influxdb_endpoint}")

        # compression
        self.compression_tested = False
        self.compression_headers = {
            "Content-Encoding": "gzip",
            "Content-Type": "application/octet-stream",
        }

        # if verification is disabled, we do not want annoying warnings
        if not Config().influxdb_verify_certificate:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    @staticmethod
    def convert_to_ts_ns(epoch: str) -> str:
        """Extend a given epoch string to pseudo nanosecond resolution."""
        return epoch + "000000000"

    def get_min_max(self) -> Tuple[Optional[datetime], Optional[datetime]]:
        """Extract earliest and latest existing timestamp from InfluxDB database."""
        self.log.info("checking existing data ranges in influxdb weather database")
        first_ts, last_ts = None, None

        for min_max_type in ("first", "last"):
            url = "".join(
                (
                    Config().influxdb_endpoint,
                    "/query?db=",
                    Config().influxdb_database_name,
                    ";",
                    f'q=select {min_max_type}(*) from "{self._type}"',
                )
            )
            self.log.debug(url)
            response = requests.post(url=url, verify=Config().influxdb_verify_certificate)
            self.log.debug(response.text)
            response.raise_for_status()
            json_response = json.loads(response.text)

            # e.g. 2019-07-27T15:20:00Z, http://strftime.org
            try:
                ts_raw = json_response["results"][0]["series"][0]["values"][0][0]
                timestamp = datetime.strptime(ts_raw, "%Y-%m-%dT%H:%M:%SZ")
            except KeyError:
                timestamp = None

            if min_max_type == "first":
                first_ts = timestamp
            else:
                last_ts = timestamp

        return first_ts, last_ts

    def determine_data_range(self) -> list:
        """Determine data range to load based on known min/max timestamp in influx."""
        first_ts, last_ts = self.get_min_max()
        actions = []

        # now = yesterday to to now
        # recent = last 500 day up to yesterday
        # historical = older that yesterday - 500 day

        if first_ts is None or last_ts is None:
            self.log.info("no data ranges found, preparing full+delta")
            actions.append("full")
            actions.append("delta")
        else:
            if first_ts >= datetime.now() - timedelta(days=500):
                self.log.info("no full data set found, preparing full")
                actions.append("full")
            if last_ts <= datetime.now() - timedelta(days=1):
                self.log.info("no delta data set found, preparing delta")
                actions.append("delta")

        if len(actions) == 0:
            self.log.info("all ok, nothing to do")

        return actions

    def check_influxdb(self):
        """Check whether InfluxDB contains available database called 'weather'."""
        self.log.debug("checking existence of influxdb weather database")
        url = "".join((Config().influxdb_endpoint, "/query?q=show databases"))
        self.log.debug(f"testing influxdb at {url}")
        response = requests.post(url=url, verify=Config().influxdb_verify_certificate)
        self.log.debug(f"got response {response.text}")
        try:
            json_response = json.loads(response.text)
        except json.decoder.JSONDecodeError:
            self.log.error("could not interpret response from influxdb server")
            return False

        return ["weather"] in json_response["results"][0]["series"][0]["values"]

    def check_or_create_influxdb(self):
        """Create the InfluxDB database called 'weather' if not existing."""
        if self.check_influxdb():
            self.log.info("influxdb weather database exists")
        else:
            self.log.info("influxdb weather database does not exists, creating")
            # TODO might not work because of missing rights
            create_url = "".join((Config().influxdb_endpoint, "/query?q=create databases weather"))
            response = requests.post(url=create_url, verify=Config().influxdb_verify_certificate)
            self.log.debug(response.text)
            response.raise_for_status()

    def _compress_data(self, payload: str):
        """Compress data with gzip if configured."""
        # https://v2.docs.influxdata.com/v2.0/write-data/developer-tools/api/
        if self.compression:
            # level 6 compression takes only 2% of overall runtime even on slow CPUs
            payload_binary = gzip.compress(data=bytes(payload, "utf-8"), compresslevel=6)
            return self.compression_headers, payload_binary

        return {}, payload

    def _send(self, payload_string):
        """Compress data and send HTTP request to InfluxDB."""
        # FIXME this call takes 45% of overall runtime, try asynchronous
        header, data = self._compress_data(payload_string)
        response = requests.post(
            url=self.url,
            data=data,
            headers=header,
            verify=Config().influxdb_verify_certificate,
        )
        response.raise_for_status()

    def post_data(
        self,
        source: Optional[str],
        component: str,
        station_id: int,
        value: str,
        epoch: str,
    ) -> int:
        """Stack up data until batch size is reached, then transfers to InfluxDB."""
        if source:
            self.weather_payload.append(
                "".join(
                    [
                        source,
                        ",component=",
                        component,
                        ",station=",
                        str(station_id),
                        " value=",
                        value,
                        " ",
                        WeatherProcessor.convert_to_ts_ns(epoch),
                    ]
                )
            )

        if len(self.weather_payload) >= self.max_messages or source is None:
            num_messages = len(self.weather_payload)
            msg = f"pushing {num_messages} {component} message(s)"
            if source is None:
                msg = "finally, " + msg
            self.log.debug(msg)
            payload_string = "\n".join(self.weather_payload)

            try:
                transmission_start = datetime.now()
                self._send(payload_string)
                self.transmission_time += datetime.now() - transmission_start

                if self.compression and not self.compression_tested:
                    self.compression_tested = True
                    self.log.info("http compression active and verified")
            except TimeoutError:
                Logger().error(f"could not connect to {Config().influxdb_database_name}")
                sys.exit(1)
            except requests.exceptions.HTTPError as error:
                if error.response.status_code == 400:
                    if self.compression_tested:
                        Logger().error(error)
                        sys.exit(1)
                    Logger().warning("http compression unsupported, falling back to uncompressed data transfer")
                    self.compression = False
                    self._send(payload_string)
                    self.compression_tested = True
                    return 0
                Logger().error(error)
                sys.exit(1)

            self.weather_payload = []
            return num_messages

        return 0

    def post_map_air_data(
        self,
        source: str,
        component: str,
        geohash: str,
        location: str,
        value: str,
        epoch: str,
    ):
        """Hand spatial air data to batched data transfer."""
        # FIXME map data is only POC
        # TODO implement real map feature (most likely not in grafana)
        if source is not None:
            # insert geo-temp,geohash=u0zh7w value=11
            self.map_air_payload.append(
                "".join(
                    [
                        source,
                        ",component=",
                        component,
                        ",geohash=",
                        geohash,
                        " value=",
                        value,
                        ',location="',
                        location,
                        '" ',
                        WeatherProcessor.convert_to_ts_ns(epoch),
                    ]
                )
            )

        if len(self.map_air_payload) >= self.max_messages or source is None:
            self.log.info(f"pushing {len(self.map_air_payload)} map_air messages")
            try:
                payload_string = "\n".join(self.map_air_payload)
                response = requests.post(
                    url=self.url,
                    data=payload_string,
                    verify=Config().influxdb_verify_certificate,
                )
                response.raise_for_status()
            except TimeoutError:
                Logger().error(f"could not connect to {Config().influxdb_database_name}")
                sys.exit(1)
            self.map_air_payload = []

    def flush_data(self):
        """Trigger data transmission."""
        backup_value = self.max_messages
        self.max_messages = 1
        self.post_data(source=None, component="", station_id=0, value="", epoch="")
        self.max_messages = backup_value

    @abstractmethod
    def parse_data(self, reader, station_id: int):
        """Open for concrete implementation based on weather type."""

    def compile_data(self):
        """Process and import all observation data."""
        # TODO remove code duplication with database
        self.log.info(f"importing {self._type} using proto_http")
        file_path = Path(Config().project_root.parent, "data", self._type)
        files = []
        for data_file in file_path.iterdir():
            if data_file.is_file():
                files.append(str(data_file.name))

        self.log.meta_create_action("upload")

        self._last_start = int(round(time.time() * 1000))
        self._last_lines = 0

        start_time = time.time()
        self._compile_data(files=files, file_path=file_path, parse_data=self.parse_data)

        self.log.info(f"imported {self._lines} lines in {round(time.time() - start_time, 1)} seconds")
        self.log.meta_end_action("upload")
        self.log.meta_set_num_records(self._lines)
        self.log.meta_end()

        self.cleanup_old_files()

    def delete_forecast_points(self, forecast_type: str, forecast_name: str = ""):
        """Remove all forecast data from InfluxDB."""
        if forecast_name == "":
            forecast_name = forecast_type

        self.log.info(f"purging forecast vales for {forecast_type}: {forecast_name} from target")
        data = {
            "db": Config().influxdb_database_name,
            "q": f'drop series from "{forecast_type}" where "component"=\'{forecast_name}-forecast\'',
        }

        response = requests.post(
            url=Config().influxdb_endpoint + "/query",
            data=data,
            verify=Config().influxdb_verify_certificate,
        )
        self.log.debug(response.text)
        response.raise_for_status()

    def insert_forecast_data(self):
        """Insert forecast data into InfluxDB."""
        # TODO split and refactor whole function
        # mosmix stations
        # https://www.dwd.de/DE/leistungen/met_verfahren_mosmix/mosmix_stationskatalog.cfg?view=nasPublication&nn=16102

        lookup = {
            "air": ["temperature", "pressure"],
            "rain": ["rain"],
            "solar": ["solar"],
            "wind": ["speed", "direction"],
        }

        config_reader = CfgReader()

        mosmix_station_ids = {}
        for station_id in Config().dwd_stations:
            mosmix_station_ids[station_id] = config_reader.get_mosmix_station_id(station_id)

        # cleanup existing forecast files and download new
        self.delete_forecast_files()
        HttpsDownloader.fetch_forecast_files(mosmix_station_ids)

        # cleanup forecast influx series
        for forecast_type in lookup[self._type]:
            self.delete_forecast_points(self._type, forecast_type)

        # reload fresh data
        folder_name = str(Config().project_root.parent / "data" / "forecast")
        folder = os.listdir(folder_name)
        for station_id, mosmix_station_id in mosmix_station_ids.items():
            self.log.info(f"checking for station {station_id}")
            for forecast_file in folder:
                if forecast_file.endswith(f"_{mosmix_station_id}.kmz"):
                    self.log.info(f"found kmz file {forecast_file}")

                    kmz_file = str(Config().project_root.parent / "data" / "forecast" / forecast_file)
                    with zipfile.ZipFile(kmz_file) as zip_file:
                        kml_file_name = ""
                        for name in zip_file.namelist():
                            zip_file.extract(name, folder_name)
                            kml_file_name = name

                    kml_file = str(Config().project_root.parent / "data" / "forecast" / kml_file_name)
                    self.log.info(f"found kml file {kml_file}")
                    kml = KmlReader(kml_file)
                    values = kml.get_forecast_values(weather_type=self._type)

                    timestamp = datetime.now().timestamp()

                    forecast_types = lookup[self._type]
                    for forecast_type in forecast_types:
                        self.log.info(f"loading {forecast_type} forecast")
                        for forecast_value in values[forecast_type]:
                            if int(forecast_value[0]) > int(timestamp):
                                self.post_data(
                                    source=self._type,
                                    component=forecast_type + "-forecast",
                                    station_id=station_id,
                                    value=str(forecast_value[1]),
                                    epoch=forecast_value[0],
                                )

                        self.flush_data()

    def _delete(self, threshold: datetime, comparator: str):
        if comparator not in (">", "<"):
            raise ValueError("invalid comparator")

        if comparator == ">":
            self.log.warning('deletion of "all data newer than" for influxdb not implemented')

        border = threshold.strftime("%Y-%m-%d")

        # (1) delete once
        # (2) install retention policy

        # https://www.reddit.com/r/influxdb/comments/fgajn5/telegrafinfluxdbgrafana_removing_old_hosts/
        # if this message occurs "ERR: cannot delete data. DB contains shards using both inmem and tsi1 indexes.
        #    ... Please convert all shards to use the same index type to delete data." then stop the service, run:
        # "influx_inspect buildtsi -datadir /var/db/influxdb/data/ -waldir/var/db/influxdb/wal/" with correct paths
        # and try again

        # (1) delete once
        # influx> delete from air where time < '2015-01-01'
        self.log.info("     deleting outdated series data")
        deletion_query = f'q=delete from "{self._type}" where time {comparator} {border}'
        url = "".join(
            (
                Config().influxdb_endpoint,
                "/query?db=",
                Config().influxdb_database_name,
                ";",
                deletion_query,
            )
        )
        self.log.debug(url)
        response = requests.post(url=url)
        self.log.debug(response.text)
        response.raise_for_status()

        # (2) install retention policy for whole weather database
        # influx> show retention policies
        # influx> drop retention policy "weather_housekeeping" on "weather"
        self.log.info("     installing retention policy for self cleanup (affects whole database)")
        retention_query = 'q=create retention policy "weather_housekeeping" on "weather" duration {num_weeks}w replication 1 default'
        url = "".join(
            (
                Config().influxdb_endpoint,
                "/query?db=",
                Config().influxdb_database_name,
                ";",
                retention_query,
            )
        )
        self.log.debug(url)
        response = requests.post(url=url, verify=Config().influxdb_verify_certificate)
        self.log.debug(response.text)
        response.raise_for_status()

        self.log.info("cleansing done.")

    def run(self):
        """Check influx database, fetch the data, process metadata and data, call housekeeping and insert forecast."""
        self.check_or_create_influxdb()
        self.fetch_data()
        self.compile_metadata()
        self.compile_data()
        self.housekeeping()

        if Config().load_forecast and self._type not in ("cloud", "visibility"):
            self.insert_forecast_data()
