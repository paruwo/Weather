from weather.protocol.http.weather_processor import WeatherProcessor


class Wind(WeatherProcessor):
    """Concrete processor for WIND data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("wind")

    def parse_data(self, reader, station_id: int):
        """Read wind data from observation file."""
        for record in reader:
            # skip header
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            timestamp = WeatherProcessor.str_to_ts(record[1].strip())
            # float, int
            speed = WeatherProcessor.fix_data_errors(record[3].strip())
            direction = WeatherProcessor.fix_data_errors(record[4].strip())

            self.post_data(
                source=self._type,
                component="speed",
                station_id=station_id,
                value=speed,
                epoch=timestamp,
            )
            self.post_data(
                source=self._type,
                component="direction",
                station_id=station_id,
                value=direction,
                epoch=timestamp,
            )

            self.print_tps()
        self.flush_data()
