from weather.protocol.http.weather_processor import WeatherProcessor


class Cloud(WeatherProcessor):
    """Concrete processor for CLOUD data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("cloud")

    def parse_data(self, reader, station_id: int):
        """Read cloudiness data from observation file."""
        for record in reader:
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            # 2018052200
            timestamp = WeatherProcessor.str_to_ts(record[1].strip())
            dull_rate = WeatherProcessor.fix_data_errors(record[4].strip())  # dull rate in 8th of sky
            dull_rate = Cloud.to_pct_string(dull_rate)

            self.post_data(
                source=self._type,
                component="dull",
                station_id=station_id,
                value=dull_rate,
                epoch=timestamp,
            )
            self.print_tps()

        self.log.debug(WeatherProcessor.fix_data_errors.cache_info())
        self.flush_data()
