from weather.protocol.http.weather_processor import WeatherProcessor


class Visibility(WeatherProcessor):
    """Concrete processor for VISIBILITY data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("visibility")

    def parse_data(self, reader, station_id: int):
        """Read wind visibility from observation file."""
        for record in reader:
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            # 2018052200
            timestamp = WeatherProcessor.str_to_ts(record[1].strip())
            visible_range = WeatherProcessor.fix_data_errors(record[4].strip())

            self.post_data(
                source=self._type,
                component="visibility",
                station_id=station_id,
                value=visible_range,
                epoch=timestamp,
            )
            self.print_tps()

        self.log.debug(WeatherProcessor.fix_data_errors.cache_info())
        self.flush_data()
