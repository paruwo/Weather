from weather.protocol.http.weather_processor import WeatherProcessor


class Rain(WeatherProcessor):
    """Concrete processor for RAIN data."""

    def __init__(self):
        """Initiate instance."""
        super().__init__("rain")

    def parse_data(self, reader, station_id: int):
        """Read rain data from observation file."""
        for record in reader:
            # skip header
            if record[0] == "STATIONS_ID":
                continue
            self._lines += 1

            timestamp = WeatherProcessor.str_to_ts(record[1].strip())
            # int, int, float
            duration = WeatherProcessor.fix_data_errors(record[3].strip())
            height = WeatherProcessor.fix_data_errors(record[4].strip())
            rain_flag = WeatherProcessor.fix_data_errors(record[5].strip())

            self.post_data(
                source=self._type,
                component="duration",
                station_id=station_id,
                value=duration,
                epoch=timestamp,
            )
            self.post_data(
                source=self._type,
                component="height",
                station_id=station_id,
                value=height,
                epoch=timestamp,
            )
            self.post_data(
                source=self._type,
                component="rain_flag",
                station_id=station_id,
                value=rain_flag,
                epoch=timestamp,
            )

            self.print_tps()

        self.log.debug(WeatherProcessor.fix_data_errors.cache_info())
        self.flush_data()
