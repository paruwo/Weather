import tomllib
from pathlib import Path

pyproject = Path(__file__).parent.parent / "pyproject.toml"
with pyproject.open("rb") as file:
    configuration_file = tomllib.load(file)
    __version__ = configuration_file["tool"]["poetry"]["version"]
