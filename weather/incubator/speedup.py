import typing
from datetime import datetime, timedelta

import pytz
from weather.util.config import Config

utc = pytz.timezone("UTC")
tz = Config().dwd_timezone
target_tz = pytz.timezone(tz)


def str_to_ts(value: str) -> str:
    """This function consumes 35% of overall runtime and cannot be cached.
    strptime() takes overall 15% compute time, replaced by datetime()."""

    # append hourly data by zero minutes if needed
    if len(value) == 10:
        value = "".join([value, "00"])

    # convert 201809231250 to proper UNIX timestamp value
    # timestamp = datetime.strptime(value, '%Y%m%d%H%M')
    timestamp = datetime(
        year=int(value[0:4]),
        month=int(value[4:6]),
        day=int(value[6:8]),
        hour=int(value[8:10]),
        minute=int(value[10:12]),
    )
    ts_tz = utc.localize(timestamp, is_dst=None).astimezone(target_tz)
    # timestamp always respective to UTC by definition
    return str(int(ts_tz.timestamp()))


def date_range(start_date: datetime, end_date: datetime) -> typing.Generator:
    for n in range(int((end_date - start_date).total_seconds())):
        yield (
            start_date + timedelta(seconds=n),
            (start_date + timedelta(seconds=n)).strftime("%Y%m%d%H%M"),
        )


if __name__ == "__main__":
    print(f"starting at {datetime.now()}")
    start_date = datetime.strptime("2018-01-01", "%Y-%m-%d")
    end_date = datetime.strptime("2018-01-03", "%Y-%m-%d")
    for _, date_str in date_range(start_date, end_date):
        str_to_ts(value=date_str)
    print(f"ending at {datetime.now()}")
