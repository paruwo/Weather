# Profiling
Create profiling file:

```bash
poetry run python -m cProfile -o trace.prof weather/incubator/speedup.py
```

Then there are two nice options, using (1) tuna or (2) dot.

## tuna

```bash
poetry install --extras "test"
poetry run tuna trace.prof
```

## dot

### Fedora

```bash
dnf install python-xdot
gprof2dot -f pstats trace.prof -o callingGraph.dot && xdot callingGraph.dot
```

### others

Not working.

```bash
poetry add xdot gprof2dot
poetry run gprof2dot -f pstats trace.prof -o callingGraph.dot
poetry run xdot callingGraph.dot
```
