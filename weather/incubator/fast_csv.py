if __name__ == "__main__":
    import pandas as pd
    from datetime import datetime

    def custom_date_parser(x):
        return datetime.strptime(x, "%Y%m%d%H%M")

    # df = pd.read_csv('data/air/10minutenwerte_TU_01544_20100101_20191231_hist.zip',
    df = pd.read_csv(
        "data/air/produkt_zehn_min_tu_20000101_20091231_01544.txt",
        sep=";",
        header=0,
        usecols=["MESS_DATUM", "PP_10"],
        # , parse_dates=['MESS_DATUM'], date_parser=custom_date_parser
    )
    df = df.where(lambda x: x != -999.0, other=0)
    # df.info()

    print(df)
