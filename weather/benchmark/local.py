"""This is the app server benchmarking part."""

import json
import timeit
from pathlib import Path

from beautifultable import STYLE_COMPACT, BeautifulTable
from weather.util.config import Config


class Local:
    """Encapsulate local benchmarking."""

    @staticmethod
    def test_ts():
        """Test local timestamp computations to determine processing speed."""
        timestamp = timeit.Timer(
            stmt='AbstractWeatherProcessor.str_to_ts("201809231250")',
            setup="from weather.abstract_weather_processor import AbstractWeatherProcessor",
        )
        ts_result = timestamp.repeat(repeat=3, number=10_000_000)

        fix = timeit.Timer(
            stmt='AbstractWeatherProcessor.fix_data_errors("-999")',
            setup="from weather.abstract_weather_processor import AbstractWeatherProcessor",
        )
        fix_result = fix.repeat(repeat=3, number=1_000_000)

        mean_ts = sum(ts_result) / float(len(ts_result))
        mean_fix = sum(fix_result) / float(len(fix_result))

        return mean_ts, mean_fix

    @staticmethod
    def print_info():
        """Show reference benchmark reference values."""
        references_file = Path(Config().project_root, "benchmark", "references.json")
        with references_file.open("r", encoding="UTF-8") as file_handler:
            references = json.load(file_handler)

        table = BeautifulTable(maxwidth=200)
        table.set_style(STYLE_COMPACT)
        table.columns.header = ["#", "ts", "fix", "sum", "CPU", "RAM", "OS", "Python"]

        for key, value in references.items():
            min_err = value["results"]["error"]["min"]
            max_err = value["results"]["error"]["max"]
            min_ts = value["results"]["ts"]["min"]
            max_ts = value["results"]["ts"]["max"]
            sum_min = value["results"]["error"]["min"] + value["results"]["ts"]["min"]
            sum_max = value["results"]["error"]["max"] + value["results"]["ts"]["max"]
            table.rows.append(
                [
                    key,
                    "{:6.2f}".format(min_ts) + " - " + "{:6.2f}".format(max_ts),
                    "{:6.3f}".format(min_err) + " - " + "{:6.3f}".format(max_err),
                    "{:6.3f}".format(sum_min) + " - " + "{:6.3f}".format(sum_max),
                    value["meta"]["cpu"],
                    value["meta"]["ram"],
                    value["meta"]["os"],
                    value["meta"]["python"],
                ]
            )
        print(table)

    @staticmethod
    def run():
        """Execute tests and shows timings."""
        Local.print_info()

        print()
        print("RUNNING BENCH...")
        print()
        timestamp, fix = Local.test_ts()

        table = BeautifulTable(maxwidth=100)
        table.set_style(STYLE_COMPACT)
        table.columns.header = ["ts", "fix", "sum"]
        table.rows.append([timestamp, fix, timestamp + fix])

        print(table)
