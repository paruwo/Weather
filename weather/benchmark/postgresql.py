"""The Postgresql benchmark part."""

from datetime import datetime, timedelta
from random import random

import psycopg2
from faker import Faker
from psycopg2.extras import execute_values
from weather.util.config import Config
from weather.util.logger import Logger


class Postgresql:
    """Benchmarking class for Postgresql data container."""

    BATCH_SIZES = (10, 100, 500, 1_000, 5_000, 10_000, 25_000)
    NUM_ROWS = 100_000

    def __init__(self):
        """Connect to the configured Postgresql database."""
        Logger().info("connecting to postgresql database")
        self.connection = psycopg2.connect(
            host=Config().db_host,
            dbname=Config().db_database,
            user=Config().db_user,
            password=Config().db_password,
            port=Config().db_port,
        )
        Logger().info("database connection ok")

    def __enter__(self):
        """Return reference to self."""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Clean up before exiting."""
        Logger().info("closing database connection")
        self.connection.rollback()
        self.connection.close()
        Logger().info("connection closed")

    def setup_tests(self):
        """Create performance test table."""
        Logger().info("setup test")
        table_sql = "create table weather_perf_ins without oids as select * from air where 1=0"
        index_sql = "alter table weather_perf_ins add primary key (station_id, effective_at)"
        cursor = self.connection.cursor()
        cursor.execute(table_sql)
        cursor.execute(index_sql)
        self.connection.commit()

    def teardown_tests(self):
        """Drop performance test table."""
        Logger().info("cleaning up")
        table_sql = "drop table if exists weather_perf_ins cascade"
        cursor = self.connection.cursor()
        cursor.execute(table_sql)
        self.connection.commit()

    def __generate_data(self, num_rows):
        """Internally generate test data to insert."""
        Logger().info(f"generating {self.NUM_ROWS} rows")
        Faker.seed(0)
        faker = Faker()

        payload = []
        start_ts = datetime.now()
        for _ in range(1, num_rows):
            payload.append(
                (
                    -999,
                    faker.date_this_year(),
                    int(30 * random()),
                    int(100 * random()),
                    int(10000 * random()),
                )
            )

        time_to_generate = datetime.now() - start_ts
        Logger().info(f"data generated in {time_to_generate}")

        return payload

    def execute(self):
        """Insert test data and keeps track of timings."""
        Logger().info("executing insertion tests")
        insert_sql = Config().db_insert_air
        cursor = self.connection.cursor()

        time_to_insert_all = timedelta(seconds=0)

        payload = self.__generate_data(self.NUM_ROWS)

        for batch_size in self.BATCH_SIZES:
            Logger().info(f"running with batch size {batch_size}")

            Logger().debug("inserting")
            start_ts = datetime.now()
            execute_values(cur=cursor, sql=insert_sql, argslist=payload, page_size=batch_size)
            self.connection.commit()
            time_to_insert = datetime.now() - start_ts
            time_to_insert_all += time_to_insert

            Logger().info(f"    time {time_to_insert}")

        Logger().info(f"overall insertion time is {time_to_insert_all}")

    def join_test(self):
        """Perform a database performance test."""

    def run(self):
        """Clean up database, setup everything freshly, then execute benchmarks and clean up again."""
        self.teardown_tests()
        self.setup_tests()
        self.execute()
        self.teardown_tests()
