from datetime import datetime, timedelta

import requests
from weather.protocol.http.rain import Rain
from weather.protocol.http.weather_processor import WeatherProcessor
from weather.util.config import Config
from weather.util.logger import Logger


class InfluxDb:
    BATCH_SIZES = (100, 500, 1_000, 2_500, 5_000, 10_000)

    def __init__(self):
        self.log = Logger()
        self.log.info("connecting to influxdb database")
        self.proc = Rain()
        self.ping()
        self.log.info("database connection ok")

    def ping(self):
        self.proc.post_data(
            source=self.proc._type,
            component="duration",
            station_id=-1,
            value="9",
            epoch="0",
        )
        self.proc.post_data(source=None, component="", station_id=-1, value="", epoch="")

    def execute(self):
        # one month ~  44.640 minutes
        # one year  ~ 527.040 minutes
        start_date = datetime(year=1980, month=1, day=1)
        end_date = datetime(year=1980, month=4, day=1)
        num_lines = 0
        for ts in self._date_range(start_date=start_date, end_date=end_date):
            self.proc.post_data(
                source=self.proc._type,
                component="duration",
                station_id=-1,
                value=str(num_lines),
                epoch=WeatherProcessor.str_to_ts(ts.strip()),
            )
            num_lines += 1
        self.proc.flush_data()
        self.log.info(f"inserted {num_lines} lines")

    def _date_range(self, start_date: datetime, end_date: datetime) -> str:
        for n in range(int((end_date - start_date).total_seconds() / 60)):
            yield (start_date + timedelta(minutes=n)).strftime("%Y%m%d%H%M")

    def setup_tests(self):
        pass

    def teardown_tests(self):
        """Drop test series for station -1."""
        # show series
        self.log.info("cleaning up influxdb")

        data = {
            "db": Config().influxdb_database_name,
            "q": f'drop series from "{self.proc._type}" where station = "-1"',
        }
        self.log.debug(data)

        response = requests.post(url=Config().influxdb_endpoint + "/query", data=data, verify=Config().influxdb_verify_certificate)
        self.log.debug(response.text)
        response.raise_for_status()

    def run(self):
        """Clean up database, setup everything freshly, then execute benchmarks and clean up again."""
        self.log.info("starting test run")
        self.teardown_tests()

        for batch_size in InfluxDb.BATCH_SIZES:
            Config().db_batch_size = batch_size
            self.log.info("--------------------------------------")
            self.log.info(f"benchmarking with batch size {batch_size}")
            start_time = datetime.now()
            self.setup_tests()
            self.execute()
            end_time = datetime.now() - start_time
            self.log.info(f"overall wall clock time was {end_time} seconds")
            self.log.info(f"pure transmission time was {self.proc.transmission_time} seconds")
            self.teardown_tests()
