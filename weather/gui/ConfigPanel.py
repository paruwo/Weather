import wx


class ConfigPanel(wx.Panel):
    def __init__(self, parent):
        super(ConfigPanel, self).__init__(parent)
        self.add_ui()

    def add_ui(self):
        sizer = wx.GridBagSizer(0, 0)

        text = wx.StaticText(self, label="Cleanup Client:")
        sizer.Add(text, pos=(0, 0), flag=wx.ALL, border=5)

        # parent, id, value, pos, size, choices[], style
        cleanup_combo = wx.ComboBox(self, choices=["Y", "N"], style=wx.CB_DROPDOWN | wx.CB_READONLY, value="Y")
        sizer.Add(cleanup_combo, pos=(0, 1), span=(1, 2), flag=wx.EXPAND | wx.ALL, border=5)

        parallel_combo = wx.ComboBox(self, choices=["Y", "N"], style=wx.CB_DROPDOWN | wx.CB_READONLY, value="Y")
        sizer.Add(parallel_combo, pos=(0, 1), span=(1, 2), flag=wx.EXPAND | wx.ALL, border=5)

        text1 = wx.StaticText(self, label="address")
        sizer.Add(text1, pos=(1, 0), flag=wx.ALL, border=5)

        tc1 = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        sizer.Add(tc1, pos=(1, 1), span=(1, 3), flag=wx.EXPAND | wx.ALL, border=5)

        text2 = wx.StaticText(self, label="age")
        sizer.Add(text2, pos=(2, 0), flag=wx.ALL, border=5)

        tc2 = wx.TextCtrl(self)
        sizer.Add(tc2, pos=(2, 1), flag=wx.ALL, border=5)

        text3 = wx.StaticText(self, label="Mob.No")
        sizer.Add(text3, pos=(2, 2), flag=wx.ALIGN_CENTER | wx.ALL, border=5)

        tc3 = wx.TextCtrl(self)
        sizer.Add(tc3, pos=(2, 3), flag=wx.EXPAND | wx.ALL, border=5)

        text4 = wx.StaticText(self, label="Description")
        sizer.Add(text4, pos=(3, 0), flag=wx.ALL, border=5)

        tc4 = wx.TextCtrl(self, style=wx.TE_MULTILINE)
        sizer.Add(tc4, pos=(3, 1), span=(1, 3), flag=wx.EXPAND | wx.ALL, border=5)
        sizer.AddGrowableRow(3)

        buttonOk = wx.Button(self, label="Ok")
        buttonClose = wx.Button(self, label="Close")

        sizer.Add(buttonOk, pos=(4, 2), flag=wx.ALL, border=5)
        sizer.Add(buttonClose, pos=(4, 3), flag=wx.ALL, border=5)

        self.SetSizerAndFit(sizer)

    def load(self):
        pass
