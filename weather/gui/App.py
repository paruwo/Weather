import wx

from weather.gui.ConfigPanel import ConfigPanel
from weather.util.config import Config


# TODO deal with deprecation warnings
# TODO get and use royalty-free icon set for menu
# TODO implement translations
# TODO make fat client or use webservices?


class App(wx.Frame):
    menu_bar = None
    app_name = "WeatherApp"
    app_string = f"{app_name} v{Config().version}"
    app_size = (800, 600)

    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, wx.ID_ANY, title=title, size=self.app_size)
        self.menu_bar = wx.MenuBar()
        self.SetMenuBar(self.menu_bar)
        self.SetDoubleBuffered(True)

        self.SetIcon(wx.Icon(Config().project_root / "resources" / "weather-icon.png"))

    def create_file_menu(self):
        file_menu = wx.Menu()
        open_item = wx.MenuItem(file_menu, id=wx.ID_OPEN, text="&Open")
        file_menu.Append(open_item)
        file_menu.AppendSeparator()
        save_item = wx.MenuItem(file_menu, id=wx.ID_SAVE, text="&Save")
        file_menu.Append(save_item)
        save_as_item = wx.MenuItem(file_menu, id=wx.ID_SAVEAS, text="Save &As")
        file_menu.Append(save_as_item)
        file_menu.AppendSeparator()
        exit_item = wx.MenuItem(file_menu, id=wx.ID_EXIT, text="E&xit")
        self.Bind(wx.EVT_MENU, self.on_exit, exit_item)
        file_menu.Append(exit_item)
        self.menu_bar.Append(file_menu, "&File")

    def create_tools_menu(self):
        tools_menu = wx.Menu()

        # Connection
        conn_menu = wx.Menu()
        conn_influx_item = wx.MenuItem(conn_menu, id=wx.ID_ANY, text="InfluxDB Target")
        conn_menu.Append(conn_influx_item)
        conn_ssh_item = wx.MenuItem(conn_menu, id=wx.ID_ANY, text="SSH Target")
        conn_menu.Append(conn_ssh_item)
        conn_dwd_item = wx.MenuItem(conn_menu, id=wx.ID_ANY, text="DWD Service Source")
        conn_menu.Append(conn_dwd_item)

        tools_menu.AppendSubMenu(conn_menu, "Co&nnectivity Check")

        # Metadata
        meta_menu = wx.Menu()
        meta_visualize_item = wx.MenuItem(meta_menu, id=wx.ID_ANY, text="&Visualize")
        meta_menu.Append(meta_visualize_item)
        meta_query_item = wx.MenuItem(meta_menu, id=wx.ID_ANY, text="&Query")
        meta_menu.Append(meta_query_item)

        tools_menu.AppendSubMenu(meta_menu, "Local &Metadata")

        # Cleanup
        cleanup_menu = wx.Menu()
        cleanup_local_item = wx.MenuItem(cleanup_menu, id=wx.ID_ANY, text="&Local Data Files")
        cleanup_menu.Append(cleanup_local_item)
        cleanup_metadata_item = wx.MenuItem(cleanup_menu, id=wx.ID_ANY, text="&Local Metadata")
        cleanup_menu.Append(cleanup_metadata_item)
        cleanup_menu.AppendSeparator()
        cleanup_ssh_item = wx.MenuItem(cleanup_menu, id=wx.ID_ANY, text="&SSH Target")
        cleanup_menu.Append(cleanup_ssh_item)
        cleanup_influxdb_item = wx.MenuItem(cleanup_menu, id=wx.ID_ANY, text="&InfluxDB Target")
        cleanup_menu.Append(cleanup_influxdb_item)

        tools_menu.AppendSubMenu(cleanup_menu, "&Cleanup")

        # Language
        language_menu = wx.Menu()
        # languageMenu.Enable(id=...)
        language_en_item = wx.MenuItem(language_menu, id=wx.ID_ANY, text="English")
        language_menu.Append(language_en_item)

        tools_menu.AppendSubMenu(language_menu, "&Language")

        self.menu_bar.Append(tools_menu, "&Tools")

    def create_run_menu(self):
        # TODO split by implemented options, e.g. also delta csv is present
        # TODO trigger live logging/monitoring
        run_menu = wx.Menu()

        full_menu = wx.Menu()
        full_air_item = wx.MenuItem(full_menu, id=wx.ID_ANY, text="&Air")
        full_menu.Append(full_air_item)
        full_wind_item = wx.MenuItem(full_menu, id=wx.ID_ANY, text="&Wind")
        full_menu.Append(full_wind_item)
        full_rain_item = wx.MenuItem(full_menu, id=wx.ID_ANY, text="&Rain")
        full_menu.Append(full_rain_item)
        full_solar_item = wx.MenuItem(full_menu, id=wx.ID_ANY, text="&Solar")
        full_menu.Append(full_solar_item)
        # fullCloudItem = wx.MenuItem(fullMenu, id=wx.ID_ANY, text='&Cloud')
        # fullMenu.Append(fullCloudItem)
        # fullMoistureItem = wx.MenuItem(fullMenu, id=wx.ID_ANY, text='&Moistrure')
        # fullMenu.Append(fullMoistureItem)
        # fullVisibilityItem = wx.MenuItem(fullMenu, id=wx.ID_ANY, text='&Visibility')
        # fullMenu.Append(fullVisibilityItem)

        run_menu.AppendSubMenu(full_menu, "&Full (csv)")

        delta_menu = wx.Menu()
        delta_air_item = wx.MenuItem(delta_menu, id=wx.ID_ANY, text="&Air")
        delta_menu.Append(delta_air_item)
        delta_wind_item = wx.MenuItem(delta_menu, id=wx.ID_ANY, text="&Wind")
        delta_menu.Append(delta_wind_item)
        delta_rain_item = wx.MenuItem(delta_menu, id=wx.ID_ANY, text="&Rain")
        delta_menu.Append(delta_rain_item)
        delta_solar_item = wx.MenuItem(delta_menu, id=wx.ID_ANY, text="&Solar")
        delta_menu.Append(delta_solar_item)

        run_menu.AppendSubMenu(delta_menu, "&Delta (http)")

        self.menu_bar.Append(run_menu, "&Run")

    def create_info_menu(self):
        info_menu = wx.Menu()
        full_item = wx.MenuItem(info_menu, id=wx.ID_ANY, text="&Help")
        info_menu.Append(full_item)
        about_item = wx.MenuItem(info_menu, id=wx.ID_ANY, text="&About")
        self.Bind(wx.EVT_MENU, self.on_about, about_item)
        info_menu.Append(about_item)

        self.menu_bar.Append(info_menu, "&Info")

    def on_exit(self, event):
        self.Close()

    def on_about(self, event):
        about_txt = "This is " + self.app_string + ", a fat client as part of Weather."
        # TODO show modules

        dialog = wx.MessageDialog(self, about_txt, "About " + self.app_string, wx.OK)
        dialog.ShowModal()
        dialog.Destroy()


app = wx.App(0)
frame = App(parent=None, title=App.app_name)

frame.create_file_menu()
frame.create_tools_menu()
frame.create_run_menu()
frame.create_info_menu()

# config panel
config_panel = ConfigPanel(frame)
config_panel.Show()

frame.SetFocus()
frame.CenterOnScreen()
frame.Show()

app.MainLoop()
