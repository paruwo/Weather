from flask import Flask, render_template, request

from weather.metadata.reader import MetaReader

app = Flask(__name__)


@app.route("/")
def hello_world():
    """Call dummy page."""
    return render_template("usage.html")


@app.route("/html/admin", methods=["GET", "POST"])
def admin():
    """Call admin page."""
    if request.method == "GET":
        return render_template("admin.html")
    return ""


@app.route("/html/dashboard", methods=["GET"])
def dashboard():
    """Call dashboard page."""
    return render_template("dashboard.html")


# TODO workflow
#    dropdown one or more station names
#    historical, recent or now
#    process


@app.route("/weather/api/v1/", methods=["GET"])
def api_overview():
    """Call API overview page."""
    return render_template("overview.html")


@app.route("/weather/api/v1/stations", methods=["GET"])
def stations():
    """Do implement."""
    return "not implemented"


@app.route("/weather/api/v1/update/all", methods=["POST"])
def update_all():
    """Do implement."""
    return "not implemented"


@app.route("/weather/api/v1/execution", methods=["GET"])
def execution():
    """Show information about all executions."""
    meta = MetaReader()
    return meta.get_execution(execution_id=request.args.get("execution_id"))


@app.route("/weather/api/v1/executions", methods=["GET"])
def executions():
    """Show information for one specific execution."""
    meta = MetaReader()
    return meta.get_executions()


if __name__ == "__main__":
    app.run()
