import json

from pony.orm import ObjectNotFound, db_session
from py_singleton import singleton
from weather.metadata.entities import Execution


@singleton
class MetaReader:
    """wrapper around metadata database for read actions"""

    @db_session
    def get_executions(self) -> str:
        """exports all executions as JSON string"""
        all_executions = Execution.select()
        execution_result = []
        for execution in all_executions:
            execution_result.append(execution.to_dict())
        return json.dumps(execution_result, sort_keys=True)

    @db_session
    def get_execution(self, execution_id: int):
        """exports one single executions as JSON string"""
        try:
            execution = Execution[execution_id].to_dict()
            return json.dumps(execution, sort_keys=True)
        except ObjectNotFound:
            return ""
