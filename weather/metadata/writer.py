from datetime import datetime

from pony.orm import commit, db_session
from py_singleton import singleton
from weather.metadata.entities import Action, Execution


@singleton
class MetaWriter:
    """wrapper around metadata database for writing actions"""

    def __init__(self, station_id: int, parallel: bool, full_delta: str, upload_type: str):
        self.__action_dict = {}
        self.__num_errors = 0
        self.__execution_id = -1
        with db_session:
            execution = Execution(
                station_id=station_id,
                parallel=parallel,
                full_delta=full_delta,
                upload_type=upload_type,
            )
            commit()
            self.__execution_id = execution.id

    @db_session
    def create_action(self, action_name: str):
        execution = Execution.get(id=self.__execution_id)
        action = Action(execution=execution, action_type=action_name)
        commit()
        self.__action_dict[action_name] = action.id

    @db_session
    def end_action(self, action_name: str):
        action_id = self.__action_dict[action_name]
        action = Action.get(id=action_id)
        action.end_at = datetime.utcnow()

    @db_session
    def end(self):
        """Finish an action and keeps timestamp."""
        execution = Execution.get(id=self.__execution_id)
        execution.end_at = datetime.utcnow()

    @db_session
    def set_full_delta(self, full_delta: str):
        """Specify of data load was for full, delta or hist data."""
        execution = Execution.get(id=self.__execution_id)
        execution.FULL_DELTA_TYPE = full_delta

    @db_session
    def set_upload_type(self, upload_type: str):
        execution = Execution.get(id=self.__execution_id)
        execution.upload_type = upload_type

    @db_session
    def set_num_records(self, num_records: int):
        execution = Execution.get(id=self.__execution_id)
        execution.num_records = num_records

    def error_correction(self):
        """Increase number of known data corrections."""
        self.__num_errors += 1

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Clean database connectivity before destruction."""
        with db_session:
            execution = Execution.get(id=self.__execution_id)
            execution.num_records_corrected = self.__num_errors
            execution.end_at = datetime.utcnow()
