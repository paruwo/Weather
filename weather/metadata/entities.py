from datetime import datetime

from pony.orm import Database, Optional, PrimaryKey, Required, Set
from weather.util.config import Config

db_file = Config().project_root / "config" / "weather.sqlite"
db = Database(provider="sqlite", filename=str(db_file), create_db=True)


class Execution(db.Entity):
    """a high-level run"""

    id = PrimaryKey(int, auto=True)
    station_id = Required(str)
    parallel = Required(bool)
    full_delta = Required(str, py_check=lambda x: x in ["full", "delta"])
    upload_type = Required(str, py_check=lambda x: x in ["csv", "http"])
    start_at = Required(datetime, sql_default="CURRENT_TIMESTAMP")
    end_at = Optional(datetime)
    num_records = Optional(int)
    num_records_corrected = Optional(int)
    actions = Set("Action")

    def to_dict(self):
        """exports the execution as dictionary"""
        actions = []
        for action in self.actions:
            actions.append(action.to_dict())

        return {
            "id": self.id,
            "station_id": self.station_id,
            "parallel": self.parallel,
            "full_delta": self.full_delta,
            "upload_type": self.upload_type,
            "start_at": self.start_at.isoformat(),
            "end_at": self.end_at.isoformat() if self.end_at is not None else "",
            "num_records": self.num_records,
            "num_records_corrected": self.num_records_corrected if self.num_records_corrected is not None else 0,
            "actions": actions,
        }


class Action(db.Entity):
    """low-level actions like download"""

    id = PrimaryKey(int, auto=True)
    action_type = Required(str)
    start_at = Required(datetime, sql_default="CURRENT_TIMESTAMP")
    end_at = Optional(datetime)
    execution = Required(Execution)

    def to_dict(self):
        """exports the action as dictionary"""
        return {
            "id": self.id,
            "action_type": self.action_type,
            "start_at": self.start_at.isoformat(),
            "end_at": self.end_at.isoformat() if self.end_at is not None else "",
        }


db.generate_mapping(create_tables=True)
