import os
import zipfile
from pathlib import Path
from typing import Dict, List, Optional

import requests
from bs4 import BeautifulSoup

from weather.util.config import Config
from weather.util.logger import Logger

# TODO https://gist.github.com/darwing1210/c9ff8e3af8ba832e38e6e6e347d9047a


class HttpsDownloader:
    """The basic facility to download and unzip weather files."""

    @staticmethod
    def __create_folder_if_not_exists(name: str) -> Path:
        target_dir = Config().project_root.parent / "data" / name
        if not target_dir.exists():
            target_dir.mkdir()
        return target_dir

    @staticmethod
    def __fetch_hist_observation_files(station_ids: List[int], weather_type: str = "air"):
        """Fetch historical observation files."""
        # assert station_ids is not None

        target_dir = HttpsDownloader.__create_folder_if_not_exists(name=weather_type)

        url = "".join((Config().dwd_http_url, Config().weather_sources[weather_type]["hist_url"]))
        Logger().info(f"using url {url}")
        file_list_raw = requests.get(url).text
        parsed_html = BeautifulSoup(file_list_raw, features="lxml")

        local_files = []
        file_list = []

        for link in parsed_html.find_all("a"):
            hist_file = link.get("href")
            if "10minutenwerte" in hist_file:
                file_list.append(hist_file)

        for hist_file in file_list:
            for station_id in station_ids:
                # 432 => _00432_
                station_search_string = "".join(("_", str(station_id).zfill(5), "_"))

                if station_search_string in hist_file:
                    local_file = target_dir / hist_file
                    Logger().info(f"downloading {hist_file} to {local_file}")

                    with local_file.open("wb") as downloaded_file:
                        response = requests.get("".join((url, hist_file)))
                        downloaded_file.write(response.content)

                    local_files.append(local_file)

        return local_files

    @staticmethod
    def fetch_observation_files(station_ids: Optional[List[int]], weather_type: str = "air", mode: str = "delta") -> List:
        """Download actual observation data files."""
        # assert weather_type is not None
        # assert mode in ('full', 'delta', 'hist')

        target_dir = HttpsDownloader.__create_folder_if_not_exists(name=weather_type)

        # base URL
        file_path = Config().dwd_http_url
        file_ending = ""
        local_files = []

        # overwrite station metadata download location
        if station_ids is None:
            mode = "delta"
        Logger().info(f"downloading observation files in {mode} mode")

        # build deep link
        if mode == "delta":
            file_path += Config().weather_sources[weather_type]["delta_url"]
            file_ending = Config().delta_file_suffix
        elif mode == "full":
            file_path += Config().weather_sources[weather_type]["full_url"]
            file_ending = Config().full_file_suffix
        elif mode == "hist":
            # Problem here is that there are different HIST files with different names each
            local_files = HttpsDownloader.__fetch_hist_observation_files(station_ids, weather_type)

        if mode in ("full", "delta"):
            if station_ids is not None:
                # download payload data for given stations
                for station_id in station_ids:
                    station_string = str(station_id).zfill(5)
                    file_name = "".join(
                        (
                            Config().weather_sources[weather_type]["file_prefix"],
                            Config().weather_sources[weather_type]["file_indicator"],
                            station_string,
                            file_ending,
                        )
                    )

                    file_location = target_dir / file_name
                    Logger().info(f"downloading {file_path + file_name} to {file_location}")
                    with file_location.open("wb") as downloaded_file:
                        response = requests.get(file_path + file_name)
                        downloaded_file.write(response.content)
                    local_files.append(file_location)
            else:
                # download station metadata
                file_name = Config().weather_sources[weather_type]["stations_url"]
                file_location = target_dir / file_name
                Logger().info(f"downloading {file_path + file_name} to {file_location}")
                with file_location.open("wb") as downloaded_file:
                    response = requests.get(file_path + file_name)
                    downloaded_file.write(response.content)
                local_files.append(file_location)

        Logger().info(f"downloaded {len(local_files)} file(s)")
        return local_files

    @staticmethod
    def fetch_forecast_files(mosmix_station_ids: Dict):
        """
        Download station metadata for forecast, which is different to obervations station data.

        :param mosmix_station_ids: stations to get data for
        :return: downloaded list of files to process
        """
        local_files = []

        target_dir = HttpsDownloader.__create_folder_if_not_exists("forecast")

        for _, mosmix_station_id in mosmix_station_ids.items():
            file_path = "".join((Config().forecast_url, mosmix_station_id, "/kml/"))
            file_name = f"MOSMIX_L_LATEST_{mosmix_station_id}.kmz"
            file_location = target_dir / file_name
            Logger().info(f"downloading {file_path + file_name} to {file_location}")
            with file_location.open("wb") as downloaded_file:
                response = requests.get(file_path + file_name)
                downloaded_file.write(response.content)
            local_files.append(file_location)

        Logger().info(f"downloaded {len(local_files)} file(s)")
        return local_files

    @staticmethod
    def unzip_file(local_files: List, sub_folder: str = "."):
        """Unzip a ZIP file into given folder, standard right where the file resides."""
        Logger().debug(f"unzipping {len(local_files)} file(s)")
        for local_file in local_files:
            Logger().debug(f"file is {local_file}")
            directory, _ = os.path.split(local_file)
            directory = Path(directory) / sub_folder
            try:
                with zipfile.ZipFile(local_file, "r") as zip_ref:
                    zip_ref.extractall(directory)
            except FileNotFoundError:
                Logger().warning(f"could not find file {local_file} to unzip removing it")
                Path(local_file).unlink()
            except zipfile.BadZipFile:
                Logger().error(f"file {local_file} is not a zip file")
        Logger().info(f"unzipped {len(local_files)} file(s)")


if __name__ == "__main__":
    HttpsDownloader.fetch_observation_files([44, 71], weather_type="rain", mode="delta")
    HttpsDownloader.fetch_forecast_files({"1": "10865", "2": "K9097"})
    HttpsDownloader.fetch_observation_files([44, 71], weather_type="rain", mode="hist")
