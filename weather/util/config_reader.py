import json
import logging
from typing import Dict

from weather.util.config import Config
from weather.util.logger import Logger


class CfgReader:
    """
    A reader for MOSMIX station metadata.

    See https://www.dwd.de/DE/leistungen/met_verfahren_mosmix/mosmix_stationskatalog.cfg?view=nasPublication&nn=16102.
    To not download same data over and over again, this file was put into this software delivery and is located
    at weather/config/mosmix_stations.cfg.
    """

    cfg_lookup: Dict
    station_json: Dict
    log: logging.Logger

    def __init__(self):
        """Initialize instance."""
        self.cfg_lookup = {}
        self.station_json = {}
        self.log = Logger()
        self.__parse()
        self.merge_into_stations_file()

    @staticmethod
    def normalize(place: str) -> str:
        """
        Try to normalize MOSMIX and observation station metadata.

        This is achieved by applying umlaut and special character transformation.
        :param place: station name
        :return: cleansed station name
        """
        ret = place.replace("-", " ")  # .replace('/', '')
        ret = ret.replace("ü", "ue").replace("ä", "ae").replace("ü", "ue").replace("ö", "oe")
        ret = ret.upper().strip(" ")
        return ret

    def __parse(self):
        """Read MOSMIX station metadata from cfg file."""
        cfg_file = Config().project_root / "config" / "mosmix_stations.cfg"
        with cfg_file.open("r", encoding="utf-8") as csv_file:
            for line in csv_file:
                if len(line) < 2 or line.startswith("TABLE") or line.startswith("=====") or line.startswith("clu"):
                    continue

                mosmix_station_id = line[12:17].strip(" ")
                station_name = line[23:44].strip(" ")
                # postprocess
                station_name = CfgReader.normalize(station_name)

                self.cfg_lookup[station_name] = mosmix_station_id
        self.log.info(f"loaded {len(self.cfg_lookup)} MOSMIX stations from config file")

    def merge_into_stations_file(self, force: bool = False):
        """
        Try to match observation and MOSMIX station names.

        If the name matches then MOSMIX station name is merged into stations.json file
        to have one single point of truth.
        :param force: if True then existing values are overwritten
        """
        station_file_name = Config().project_root / "config" / "stations.json"
        num_merges = 0

        # read once
        self.log.debug("reading stations.json")
        with station_file_name.open("r", encoding="utf-8") as station_file:
            self.station_json = json.load(station_file)

        # process
        self.log.debug("matching station names")
        for station_id in self.station_json:
            station_name = self.station_json[station_id]["name"]
            station_name = CfgReader.normalize(station_name)

            # abort if MOSMIX entries are already there
            if force is False and self.station_json[station_id]["mosmix_id"] is not None:
                self.log.info("MOSMIX stations are already present, no need to integrate into stations.json")
                return

            if station_name in self.cfg_lookup:
                self.station_json[station_id]["mosmix_id"] = self.cfg_lookup[station_name]
                self.log.debug(f"found station match for {station_name}")
                num_merges += 1
            else:
                self.station_json[station_id]["mosmix_id"] = None

        # write back
        self.log.debug("writing back enhanced stations.json")
        with station_file_name.open("w") as file_handler:
            json.dump(obj=self.station_json, fp=file_handler, indent=2)

        self.log.info(f"merged {num_merges} MOSMIX stations into stations.json")

    def get_mosmix_station_id(self, station_id: int) -> str:
        """
        Basically THE usable entity of this class.

        :param station_id: numerical observation station_id
        :return:
        """
        return self.station_json[str(station_id)]["mosmix_id"]


if __name__ == "__main__":
    reader = CfgReader()
    reader.merge_into_stations_file(force=True)

    reader.log.debug(reader.get_mosmix_station_id(3379))
