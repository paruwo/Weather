import importlib.util
import logging

from py_singleton import singleton
from weather.util.config import Config


@singleton
class Logger:
    """Project logging facility, cares about coloring etc."""

    has_meta_db: bool
    meta_write = None

    def __init__(self):
        """Initiate instance."""
        logging.basicConfig(level=Config().log_level)
        self.has_meta_db = Config().feature_metadata

        module_coloredlogs = importlib.util.find_spec("coloredlogs")
        if module_coloredlogs is not None:
            import coloredlogs

            coloredlogs.install(milliseconds=True)
            coloredlogs.set_level(Config().log_level)
        else:
            logging.warning("could not find coloredlogs")

    def add_meta_db(self, full_delta, upload_type=None):
        """Do implmenet."""
        pass

    def meta_set_full_delta(self, full_delta):
        """Inform metastore about data loading type."""
        if self.meta_write is not None:
            self.meta_write.set_full_delta(full_delta)

    def meta_set_upload_type(self, upload_type):
        """Inform metastore about data upload type."""
        if self.meta_write is not None:
            self.meta_write.set_upload_type(upload_type)

    def meta_create_action(self, text):
        """Inform metastore that something starts."""
        if self.meta_write is not None:
            self.meta_write.create_action(text)

    def meta_end_action(self, text):
        """Inform metastore that something ends."""
        if self.meta_write is not None:
            self.meta_write.end_action(text)

    def meta_error_correction(self):
        """Inform metastore that data correction happened."""
        if self.meta_write is not None:
            self.meta_write.error_correction()

    def meta_set_num_records(self, num_records):
        """Inform metastore about moved data records."""
        if self.meta_write is not None:
            self.meta_write.set_num_records(num_records)

    def meta_end(self):
        """Inform metastore about end of messages."""
        if self.meta_write is not None:
            self.meta_write.end()

    def debug(self, text):
        """Encapsulate DEBUG."""
        logging.debug(text)

    def info(self, text):
        """Encapsulate INFO."""
        logging.info(text)

    def warning(self, text):
        """Encapsulate WARNING."""
        logging.warning(text)

    def error(self, text):
        """Encapsulate ERROR."""
        logging.error(text)
