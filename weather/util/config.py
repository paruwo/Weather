import tomllib
from pathlib import Path
from typing import Dict, List

from py_singleton import singleton

from weather import __version__


@singleton
class Config:
    """Singleton configuration to access weather.toml and version.py programmatically."""

    project_root: Path
    version: str

    # CONFIGURATION
    ssh_hostname: str
    ssh_port: int
    ssh_authentication_method: str
    ssh_username: str
    ssh_password: str
    ssh_keyfile: str
    ssh_compression: bool

    # influxdb
    influxdb_endpoint: str
    influxdb_database_name: str

    # database
    db_vendor: str
    db_host: str
    db_port: int
    db_user: str
    db_password: str
    db_database: str
    db_batch_size: int
    # database housekeeping
    db_housekeeping_partitions_sql: str
    db_insert_air: str
    db_insert_rain: str
    db_analyze_table: str
    db_refresh_mv: str

    # DWD
    base_url: str
    dwd_stations: List
    dwd_timezone: str

    log_level: str
    parallel: bool
    feature_metadata: bool
    feature_webservice: bool

    housekeeping_enable: bool
    keep_months: int

    # CONSTANTS
    weather_types: List
    delta_file_prefix: str
    full_file_prefix: str
    delta_file_suffix: str
    full_file_suffix: str
    weather_sources: Dict

    def __init__(self):
        self.project_root = Path(__file__).parents[1]
        self.version = __version__
        # self.version = "0.1.3"
        self._parse_config_file()
        self._parse_constant_file()
        self._parse_database_file()

    def _parse_database_file(self):
        postgres_file = Path(self.project_root, "resources", "postgres_queries.toml")
        with postgres_file.open("rb") as _file:
            postgres_parser = tomllib.load(_file)

        # general
        self.db_housekeeping_partitions_sql = postgres_parser["partitions"]
        self.db_insert_air = postgres_parser["insert_air"]
        self.db_insert_rain = postgres_parser["insert_rain"]
        self.db_analyze_table = postgres_parser["analyze_table"]
        self.db_refresh_mv = postgres_parser["refresh_mv"]
        self.db_cluster_table = postgres_parser["cluster_table"]

    def _parse_config_file(self):
        config_file = Path(self.project_root, "config", "weather.toml")
        config_parser = None
        with config_file.open("rb") as _file:
            config_parser = tomllib.load(_file)

        # general
        self.log_level = config_parser["general"]["loglevel"]
        self.cleanup_client = config_parser["general"]["cleanup_client"]
        self.cleanup_retention_days = config_parser["general"]["cleanup_retention_days"]
        self.parallel = config_parser["general"]["parallel"]
        self.http_batch_size = config_parser["general"]["http_batch_size"]
        self.http_compression = config_parser["general"]["http_compression"]
        self.load_forecast = config_parser["general"]["load_forecast"]

        # housekeeping
        self.housekeeping_enable = config_parser["housekeeping"]["enable"]
        self.keep_months = config_parser["housekeeping"]["keep_months"]

        # feature
        self.feature_webservice = config_parser["features"]["webservice"]
        self.feature_metadata = config_parser["features"]["metadata"]

        # ssh
        # self.ssh_hostname = config_parser['ssh']['hostname']
        # self.ssh_port = int(config_parser['ssh']['port'])
        # self.ssh_authentication_method = config_parser['ssh']['authentication_method']
        # self.ssh_username = config_parser['ssh']['username']

        # if self.ssh_authentication_method != 'password':
        #    raise Exception('authentication method must be one of "password"')
        # self.ssh_password = config_parser['ssh']['password']

        # self.ssh_compression = config_parser['ssh']['compression']

        # influxdb
        self.influxdb_endpoint = config_parser["influxdb"]["endpoint"]
        self.influxdb_database_name = config_parser["influxdb"]["database_name"]
        self.influxdb_verify_certificate = config_parser["influxdb"]["verify_certificate"]

        # database
        self.db_vendor = config_parser["database"]["vendor"]
        self.db_host = config_parser["database"]["host"]
        self.db_port = config_parser["database"]["port"]
        self.db_user = config_parser["database"]["user"]
        self.db_password = config_parser["database"]["password"]
        self.db_database = config_parser["database"]["database"]
        self.db_batch_size = config_parser["database"]["batch_size"]
        self.db_truncate = config_parser["database"]["truncate"]
        self.ssl_mode = config_parser["database"]["ssl_mode"]

        # dwd
        self.dwd_timezone = config_parser["dwd"]["timezone"]
        self.dwd_stations = config_parser["dwd"]["stations"]
        self.dwd_delta_timerange = config_parser["dwd"]["delta_timerange"]
        self.dwd_full_timerange = config_parser["dwd"]["full_timerange"]

    def _parse_constant_file(self):
        constant_file = Path(self.project_root, "config", "constants.toml")
        with constant_file.open("rb") as _file:
            constant_parser = tomllib.load(_file)

        self.weather_types = constant_parser["general"]["weather_types"]
        self.dwd_http_url = constant_parser["general"]["http_url"]

        self.delta_file_suffix = constant_parser["general"]["delta_suffix"]
        self.full_file_suffix = constant_parser["general"]["full_suffix"]
        self.hist_file_suffix = constant_parser["general"]["hist_suffix"]

        self.weather_sources = {}
        for weather_type in self.weather_types:
            self.weather_sources[weather_type] = {}

            self.weather_sources[weather_type]["frequency"] = "".join(
                (
                    constant_parser[weather_type]["frequency"],
                    "/",
                    constant_parser[weather_type]["frequency"],
                )
            )

            self.weather_sources[weather_type]["delta_url"] = "".join(
                (
                    constant_parser[weather_type]["frequency"],
                    "/",
                    constant_parser[weather_type]["delta_url"],
                )
            )

            self.weather_sources[weather_type]["full_url"] = "".join(
                (
                    constant_parser[weather_type]["frequency"],
                    "/",
                    constant_parser[weather_type]["full_url"],
                )
            )

            self.weather_sources[weather_type]["hist_url"] = "".join(
                (
                    constant_parser[weather_type]["frequency"],
                    "/",
                    constant_parser[weather_type]["hist_url"],
                )
            )

            self.weather_sources[weather_type]["file_indicator"] = constant_parser[weather_type]["file_indicator"]

            self.weather_sources[weather_type]["stations_url"] = constant_parser[weather_type]["stations_url"]

            self.weather_sources[weather_type]["file_prefix"] = constant_parser[weather_type]["file_prefix"]

        self.forecast_url = constant_parser["general"]["forecast_url"]


if __name__ == "__main__":
    print("version is", Config().version)
