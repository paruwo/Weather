import csv
import json
import os
import re
import time
from abc import ABC, abstractmethod
from datetime import datetime, timedelta
from functools import cache
from math import ceil
from pathlib import Path
from typing import Dict, List, Tuple

import pytz

# from geohash2 import geohash
import geohash_tools as geohash

from weather.util.config import Config
from weather.util.https_downloader import HttpsDownloader
from weather.util.logger import Logger


class AbstractWeatherProcessor(ABC):
    """
    The central weather processing facility which basically does everything except data mangling for the concrete case.
    """

    _type: str
    _lines: int
    _last_start: int
    _last_lines: int
    _full_delta: str
    stations: Dict
    station_file: Path
    log: Logger
    milliseconds: int

    def __init__(self, weather_type: str, full_delta: str = "delta"):
        self._type = weather_type
        self._lines = 0
        self._last_start = 0
        self._last_lines = 0
        self._full_delta = full_delta
        self.stations = {}
        self.log = Logger()
        self.milliseconds = 1000
        self.station_file = Config().project_root / "config" / "stations.json"

        Logger().debug(f"created object to process {self._type}")
        Logger().add_meta_db(full_delta=full_delta)

    @staticmethod
    def dates_from_file_name(file_name: str) -> Tuple[datetime, datetime]:
        """extract validity from and to dates from a given file name, file name must
        contain yyyymmdd_yyyymmdd."""
        match = re.search(r"(\d{8})(_)(\d{8})", file_name)
        dt1 = match.group(1)
        dt2 = match.group(3)

        Logger().debug(f"file name is {file_name}, dt1={dt1}, dt2={dt2}")
        return datetime.strptime(dt1, "%Y%m%d"), datetime.strptime(dt2, "%Y%m%d")

    @staticmethod
    def filter_ten_minutes(file_name: str, full_delta: str) -> bool:
        """
        Based on the given file name and full or delta loading mechanism, it determines whether
        the file has to be considered. The name "ten minutes" was taken from DWD interval of
        having data with 10 minute resolution.
        For delta: True if valid from / to differ less than 10 days
        For full: True if valid from / to differ less than 549 days
        For delta: True if valid from / to differ more than than 548 days
        """
        # hist: produkt_zehn_min_rr_20100101_20181231_05111.txt > 549 days
        # full: produkt_zehn_min_rr_20171226_20190628_05111.txt = 549 days
        # delta: produkt_zehn_now_rr_20190629_20190630_05111.txt < 10 days

        Logger().debug(f"filename to check is {file_name}")
        from_dt, to_dt = AbstractWeatherProcessor.dates_from_file_name(file_name)
        Logger().debug(f"from={from_dt}, to={to_dt}, delta={to_dt - from_dt}")

        if full_delta == "delta" and to_dt - from_dt < timedelta(days=Config().dwd_delta_timerange):
            Logger().debug(f"found delta file {file_name}")
            return True

        if full_delta == "full" and timedelta(days=Config().dwd_delta_timerange) <= (to_dt - from_dt) <= timedelta(days=549):
            Logger().debug(f"found full file {file_name}")
            return True

        if full_delta == "hist" and to_dt - from_dt > timedelta(days=549):
            Logger().debug(f"found hist file {file_name}")
            return True

        Logger().debug(f"file {file_name} does not fit")
        return False

    @staticmethod
    def filter_hours(file_name: str) -> bool:
        """Here we additionally have to filter out all unnecessary files, too."""
        # cloud, visibility
        if file_name.find("produkt_n_stunde_") > -1:
            return True
        if file_name.find("produkt_vv_stunde_") > -1:
            return True
        return False

    def filter_file_names(self, file_name: str) -> bool:
        """
        Determines for a given payload file name whether it belongs to full, delta or hist data range.
        :param file_name:
        :return: True if matches to the type that is set up, False else
        """
        if "_zehn_" in file_name:
            return self.filter_ten_minutes(file_name, self._full_delta)
        if "_stunde_" in file_name:
            return self.filter_hours(file_name)
        return False

    # @staticmethod
    # def file_age_in_seconds(file: Path) -> int:
    #    return int(time.time() - file.lstat().st_ctime)

    # def check_files_to_download(self, station_ids: List, weather_type: str) -> bool:
    #    return True

    def fetch_data(self):
        """
        Downloads per HTTPS and unzips observation payload files.
        :return:
        """
        downloaded = HttpsDownloader.fetch_observation_files(
            station_ids=Config().dwd_stations,
            weather_type=self._type,
            mode=self._full_delta,
        )
        HttpsDownloader.unzip_file(local_files=downloaded)

    def __fetch_station_metadata(self):
        """
        Download and unzip observation station metadata.
        :return:
        """
        HttpsDownloader.fetch_observation_files(station_ids=None, weather_type=self._type, mode=self._full_delta)

    def __read_station_metadata_file(self) -> bool:
        """
        Try to read existing observation station json file.
        :return: True if found and processed, False else
        """
        station_file = Path(self.station_file)
        if station_file.is_file():
            self.log.info("found station metadata file, no station processing needed")
            with station_file.open(mode="r", encoding="iso-8859-1") as file_handler:
                self.log.info("loading station metadata")
                self.stations = json.load(file_handler)
                return True
        return False

    def _compile_data(self, files: List, file_path: Path, parse_data) -> List:
        """
        Central function to process observation files.
        :param files: list of payload files to process
        :param file_path: download location of files
        :param parse_data: function to execution for data processing (e.g. specific for air)
        :return: list of processed files in order to become deleted
        """
        files_to_delete = []
        for observation_file in files:
            self.log.debug(f"checking {observation_file}")
            for station in Config().dwd_stations:
                # station __345 -> 00345
                station = str(station).zfill(5)
                self.log.debug(f"check files for station {station}")
                if station in observation_file and self.filter_file_names(observation_file):
                    self.log.info(f"importing {observation_file}")
                    candidate = Path(file_path / observation_file)
                    with candidate.open("r", encoding="UTF-8") as csv_file:
                        reader = csv.reader(csv_file, delimiter=";")
                        parse_data(reader, station_id=int(station))

                    csv_file.close()
                    files_to_delete.append(observation_file)
        return files_to_delete

    def compile_metadata(self):
        """
        Tries to read existing observation station metadata. If it is not present, then this data
        is downloaded, processed and stored as station.json. Whenever you have errors in this file
        you can delete it and it will be reconstructed.
        """
        # try to read from local file cache
        if self.__read_station_metadata_file():
            return

        self.__fetch_station_metadata()
        self.log.info(f"importing {self._type} stations")
        # per convention use NOW files
        file_name = "".join(
            (
                "zehn_now",
                Config().weather_sources[self._type]["file_indicator"],
                "Beschreibung_Stationen.txt",
            )
        )
        file_path = Config().project_root.parent / "data" / file_name

        skip_lines = 2
        stations = {}
        with file_path.open(mode="r", encoding="iso-8859-1") as fix_file:
            for line in fix_file:
                if skip_lines > 0:
                    skip_lines -= 1
                    continue
                station_id = int(line[0:6].strip())
                station_name = line[61:102].strip()
                latitude = line[43:53].strip()
                longitude = line[53:61].strip()
                geo_hash = geohash.encode(float(latitude), float(longitude))
                self.log.debug(f"latitude={latitude} longitude={longitude} geohash={longitude}")

                stations[station_id] = {
                    "name": station_name,
                    "latitude": latitude,
                    "longitude": longitude,
                    "geohash": geo_hash,
                }

        with self.station_file.open("w") as file_handler:
            json.dump(obj=stations, fp=file_handler, indent=2)

        if not self.__read_station_metadata_file():
            raise RuntimeWarning("could not process metadata file")

    def make_full(self):
        """Sets the mode to FULL (last ~549 day) data processing, compared to delta or hist"""
        self._full_delta = "full"
        Logger().meta_set_full_delta(self._full_delta)
        return self

    def make_delta(self):
        """Sets the mode to DELTA (last 10 days) data processing, compared to full or hist"""
        self._full_delta = "delta"
        Logger().meta_set_full_delta(self._full_delta)
        return self

    def make_hist(self):
        """Sets the mode to HIST (> last 548 day) data processing, compared to full or delta"""
        self._full_delta = "hist"
        Logger().meta_set_full_delta(self._full_delta)
        return self

    def cleanup_old_files(self):
        """
        Depending on parameter cleanup_client, old files are physically removed from data folder.
        The maximum age of a file is configured in parameter cleanup_retention_days. All files
        with creation date older than this are deleted.
        """
        if not Config().cleanup_client:
            return

        path = Config().project_root.parent / "data"
        for weather_type in Config().weather_types:
            current_folder = Path(path / weather_type)

            if not current_folder.exists():
                self.log.debug(f"{current_folder} not existing")
                continue

            self.log.info(f"checking {current_folder} for old candidates to remove")
            for observation_file in current_folder.iterdir():
                if datetime.now() - datetime.strptime(time.ctime(observation_file.stat().st_ctime), "%c") > timedelta(
                    days=Config().cleanup_retention_days
                ):
                    self.log.info(f"deleting {observation_file} from {time.ctime(observation_file.stat().st_ctime)}")
                    observation_file.unlink()

    def print_tps(self, check_every: int = 10000, update_frequency_ms: int = 300):
        """
        Prints the transactions per second (TPS) with regards to internal data processing to output log.
        Data transfer is not included, so it's an indicator for efficiency only.

        You can control frequency of the output by combining the two parameters. Sensible defaults are set.
        :param check_every: num of rows to process before regarding TPS output
        :param update_frequency_ms: number of ms that have to tick before we think about output
        """
        # check throughput, but not too often
        if self._lines % check_every == 0 and (int(round(time.time() * self.milliseconds)) - self._last_start) >= update_frequency_ms:
            tps = round((self._lines - self._last_lines) * self.milliseconds / update_frequency_ms)
            self.log.info(f"{self._type} {tps} TPS")
            self._last_start = int(round(time.time() * self.milliseconds))
            self._last_lines = self._lines

    @staticmethod
    @cache
    def fix_data_errors(value: str) -> str:
        """
        In case of bad data the DWD sends -999 values. This in mind we replace it by 0.
        :param value: string value to check on errors
        :return: 0 if value is -999, else value
        """
        if value != "-999":
            return value

        Logger().meta_error_correction()
        return "0"

    @staticmethod
    @cache
    def str_to_ts(value: str) -> str:
        """
        Converts date text like 201809231250 = yyyy mm dd hh24 mi into UNIX epoch with seconds.
        This is an expensive operation in comparison to many others.
        Memoization (result caching) definitely helps, if multiple stations or multiple weather
        types are processed.

        :param value: timestamp in format YYYYMMDDHH24MM
        :return: UNIX timestamp, time zone conversions do not work properly
        """
        # append zero minutes if needed, 2018092312__ -> 201809231200
        value = value.ljust(12, "0")

        # convert 201809231250 to proper UNIX timestamp value
        if (time_zone := Config().dwd_timezone) != "UTC":
            # basic constructor way faster than strptime() call
            timestamp = datetime(
                year=int(value[0:4]),
                month=int(value[4:6]),
                day=int(value[6:8]),
                hour=int(value[8:10]),
                minute=int(value[10:12]),
            )
            utc = pytz.timezone("UTC")
            ts_tz = utc.localize(timestamp, is_dst=None).astimezone(pytz.timezone(time_zone))
            # timestamp always respective to UTC by definition
            return str(int(ts_tz.timestamp()))

        timestamp = datetime(
            year=int(value[0:4]),
            month=int(value[4:6]),
            day=int(value[6:8]),
            hour=int(value[8:10]),
            minute=int(value[10:12]),
        )
        return str(int(time.mktime(timestamp.utctimetuple())))

    @staticmethod
    @cache  # 0...8, -999
    def to_pct_string(dull_eights: str) -> str:
        """
        Convert dull value of 0...8 of 8 parts to fractional value.
        :param dull_eights: dull value 0..8
        :return: fractioned values in 8th; i.e. 0, 1/8, ..., 8/8
        """
        dull = float(dull_eights)
        if 0.0 <= dull <= 8.0:
            return str(dull / 8)
        return "0"

    @abstractmethod
    def run(self):
        """Placeholder for wrapper around everything, to be implemented by concrete classes."""

    @abstractmethod
    def compile_data(self):
        """Placeholder for core procedure which does all magic."""

    def delete_forecast_files(self):
        """
        Removes all .kmz forecast files from download folder.
        :return:
        """
        folder_name = str(Config().project_root.parent / "data")
        folder = os.listdir(folder_name)

        for item in folder:
            if item.endswith(".kml") or item.endswith(".kmz"):
                self.log.debug(f"deleting {os.path.join(folder_name, item)}")
                Path(Path(folder_name) / item).unlink()

    @abstractmethod
    def delete_forecast_points(self, forecast_type: str, forecast_name=None):
        """Placeholder for concrete database housekeeping with respect to forecast data."""

    @abstractmethod
    def insert_forecast_data(self):
        """Placeholder for concrete database procedure to insert forecast data."""

    @abstractmethod
    def _delete(self, threshold: datetime, comparator: str):
        """Placeholder for concrete database housekeeping."""

    def _delete_before(self, upper_threshold: datetime):
        """Syntactical sugar to delete database points before a given high threshold."""
        self._delete(threshold=upper_threshold, comparator="<")

    def _delete_after(self, lower_threshold: datetime):
        """Syntactical sugar to delete database points after a given low threshold."""
        self._delete(threshold=lower_threshold, comparator=">")

    def housekeeping(self):
        """Managing helper for database deletion procedures."""
        if not Config().housekeeping_enable:
            self.log.warning("database housekeeping is disabled")
            return

        # calculate with 31 days per month to bo on the safe side
        delta_days = timedelta(days=ceil(31 * Config().keep_months))
        threshold = datetime.now() - delta_days
        self.log.info(f"running houskeeping - keep last {Config().keep_months} month(s), delete all before {threshold}")
        self._delete_before(threshold)
