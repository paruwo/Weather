#!/usr/bin/env groovy

// https://www.jenkins.io/doc/book/pipeline/syntax/

pipeline {
    agent {
        label('linux')
    }

    options {
        buildDiscarder logRotator(
            daysToKeepStr: '14',
            numToKeepStr: '10'
        )
    }

    stages {
        stage('Checkout') {
            steps {
                git url: 'https://codeberg.org/paruwo/Weather.git'
            }
        }

        stage('Setup') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''pip3 install poetry
                          poetry install --extras "quality test"
                        '''
                }
            }
        }

        stage('Lint') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''poetry run ruff check'''
                }
                /*withPythonEnv('python3.11') {
                    sh '''poetry run pylint --exit-zero weather > pylint.log'''
                }
                recordIssues(
                    tool: pyLint(pattern: 'pylint.log'),
                    unstableTotalAll: 20,
                    failedTotalAll: 30
				)*/
            }
        }

        stage('Test Execution') {
            steps {
                withPythonEnv('python3.11') {
                    sh 'cd weather/test && poetry run pytest --junitxml=./result.xml test_AbstractWeatherProcessor.py'
                }
            }
            post {
                always {
                    junit 'weather/test/result.xml'
                }
            }
        }

        stage('Test Coverage') {
            steps {
                withPythonEnv('python3.11') {
                    sh '''cd weather/test
                          poetry run coverage run --source=../ -m pytest test_AbstractWeatherProcessor.py
                          poetry run coverage xml -o coverage.xml
                          # coverage html
                       '''
                    }
            }
            post{
                always{
                    step([$class: 'CoberturaPublisher',
                           autoUpdateHealth: false,
                           autoUpdateStability: false,
                           coberturaReportFile: 'weather/test/coverage.xml',
                           failNoReports: false,
                           failUnhealthy: false,
                           failUnstable: false,
                           maxNumberOfBuilds: 10,
                           onlyStable: false,
                           sourceEncoding: 'ASCII',
                           zoomCoverageChart: false]
                    )
                }
            }
        }

    }
}
