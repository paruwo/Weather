#!/bin/sh

# 1. grab data
# 2. reformat to importable csv
# 3. clean existing data and bulk import

###############################
### 1. grab data
###############################
# initial load
# curl ftp://ftp-cdc.dwd.de/pub/CDC/observations_germany/climate/10_minutes/precipitation/recent/10minutenwerte_nieder_03379_akt.zip --output data.zip

curl ftp://ftp-cdc.dwd.de/pub/CDC/observations_germany/climate/10_minutes/precipitation/now/10minutenwerte_nieder_03379_now.zip --output data.zip
# tar -xzf data.zip
unzip data.zip
rm data.zip
mv prod*.txt data.tmp


# remove file header
tail -n +2 < data.tmp > data.txt
rm data.tmp


###############################
# 2. reformat
###############################

# prepare importable result file for influxdb
echo "# DDL" > data_csv.dat
echo "# DML" >> data_csv.dat
echo "# CONTEXT-DATABASE: weather" >> data_csv.dat


# input: timestamp, example 201804090240
# output: unix epoch in nanoseconds
#### FreeBSD-specifics so far
if [[ `uname -s` -eq "Linux" ]]; then
  echo 'unsupported'
  exit 0
fi
if [[ `uname -s` -eq "FreeBSD" ]]; then
  ts2epoch() {
    epoch=`date -j -f "%Y%m%d%H%M" -d "$1" +%s`
    echo "${epoch}000000000"
  }
fi


IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
# row=0
for i in $(cat < "data.txt"); do
  #echo $i
  #echo $row
  ts=`echo "$i" | cut -d ";" -f 2`
  #epoch=`date -j -f "%Y%m%d%H%M" "$ts" +%s`
  #epoch="${epoch}000000000"
  epoch=$(ts2epoch $ts)

  h=`echo "$i" | cut -d ";" -f 4 | sed 's/ //g'`
  dur=`echo "$i" | cut -d ";" -f 5 | sed 's/ //g'`

  echo "rain,component=height value=$h $epoch" >> data_csv.dat
  echo "rain,component=duration value=$dur $epoch" >> data_csv.dat

  # also works but very slow for bulk import
  # curl -i -XPOST 'http://192.168.0.6:8086/write?db=weather' --data-binary "rain,component=height value=$h $epoch"
  # curl -i -XPOST 'http://192.168.0.6:8086/write?db=weather' --data-binary "rain,component=duration value=$dur $epoch"

  # row = `expr $row +1`
done



###############################
# 3.
###############################

# do delete existing data points for time period
# first and last epoch (201809152350)
minepoch=`cut -d ";" -f2 data.txt | sort -n | head -1`
maxepoch=`cut -d ";" -f2 data.txt | sort -n | tail -1`

# echo "delete data from $minepoch ... $maxepoch"
# influx -execute "delete where time >= $(ts2epoch $minepoch) and time <= $(ts2epoch $maxepoch)" -database=weather

# bulk import (few seconds)
# influx -import -path=data_csv.dat
