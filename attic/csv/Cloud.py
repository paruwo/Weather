from abc import ABC

from weather.protocol.csv.WeatherProcessor import WeatherProcessor


class Cloud(WeatherProcessor, ABC):
    """Implement csv data processing for cloudiness data."""

    def __init__(self):
        """Initiate instance."""
        super(Cloud, self).__init__("cloud")

    def parse_data(self, reader, station, output_file):
        """Read cloudiness data from csv observation file."""
        for line in reader:
            if line[0] == "STATIONS_ID":
                continue
            self._lines += 1

            ts = WeatherProcessor.str_to_ts(line[1])
            dull_rate = WeatherProcessor.fix_data_errors(line[4])  # dull rate in 8th of sky
            dull_rate = Cloud.to_pct_string(dull_rate)

            self.emit_data(source=self._type, output_file=output_file, station=station, component="dull", value=dull_rate, epoch=ts)

            self.print_tps()
