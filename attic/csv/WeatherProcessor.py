import csv
import os
import time
from abc import abstractmethod
from pathlib import Path

import paramiko

from weather.abstract_weather_processor import AbstractWeatherProcessor
from weather.util.config import Config


class WeatherProcessor(AbstractWeatherProcessor):
    """A weather processor that can create bulk files to be imported by InfluxDB."""

    def __init__(self, weather_type):
        """Initiate instance."""
        AbstractWeatherProcessor.__init__(self, weather_type=weather_type)
        self.output_path = str(Config().project_root.parent / "data")
        self.log.meta_set_upload_type("csv")

    @staticmethod
    def emit_data(source, output_file, component, station, value, epoch):
        """Append preprocessed data to file for later InfluxDB import."""
        output_file.write("".join([source, ",component=", component, ",station=", station, " value=", value, " ", epoch, "\n"]))

    @abstractmethod
    def parse_data(self, reader, station, output_file):
        """Process data, to be implemented by specialized classes."""
        pass

    @staticmethod
    def dos2unix(input_file):
        """Convert a datafile from DOS to UNIX endings."""
        data_file = Config().project_root.parent / "data" / input_file

        with data_file.open("rb") as infile:
            content = infile.read()
        with Path(data_file + "_tmp").open("wb") as output:
            for line in content.splitlines():
                output.write(line + b"\n")

        data_file.unlink()
        Path(Config().project_root.parent / "data" / input_file + "_tmp").rename(data_file)

    def compile_data(self):
        """
        Transform raw incoming weather files into a file that influxdb can directly read.

        Usually even this naive approach is very fast - 30k rec/sec.
        """
        self.log.meta_create_action("compile")

        output_file_name = "".join(["output_", self._type, "_", self._full_delta, ".dat"])
        output_file = Path(Path(self.output_path) / output_file_name).open("w")

        # add header
        output_file.write("# DDL\n")
        # CREATE RETENTION POLICY twoyears ON weather DURATION 2y REPLICATION 1
        output_file.write("# DML\n")
        output_file.write("# CONTEXT-DATABASE: weather\n")
        # CONTEXT-RETENTION-POLICY: twoyears

        self.log.info(f"generating importing file for {self._type} using protocol CSV")

        self._last_start = int(int(round(time.time() * 1000)))
        self._last_lines = 0

        source_file_name = "".join(["input_", self._type, "_", self._full_delta, ".txt"])

        with Path(Path(self.output_path) / source_file_name).open("r") as csv_file:
            reader = csv.reader(csv_file, delimiter=";", skipinitialspace=True)
            self.parse_data(reader=reader, station=str(int(Config().dwd_station_id)), output_file=output_file)
        csv_file.close()
        output_file.close()
        self.log.info(f"preprocessed {self._lines} lines")

        # clean original txt file if configured
        self.cleanup_old_files()
        self.log.meta_end_action("compile")

    def transfer_and_import(self):
        """
        Copy mangled file to InfluxDB server and import there locally.

        In detail: connect to the influxdb server, upload the previously converted data file,
        trigger a character set conversion and starts the data import.
        """
        output_file_name = "output_" + self._type + "_" + self._full_delta + ".dat"

        self.log.meta_create_action("dos2unix")
        WeatherProcessor.dos2unix(output_file_name)
        self.log.meta_end_action("dos2unix")

        self.log.meta_create_action("upload")
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh_client.connect(
                hostname=Config().ssh_hostname,
                port=Config().ssh_port,
                username=Config().ssh_username,
                password=Config().ssh_password,
                compress=Config().ssh_compression,
            )
        except (paramiko.BadHostKeyException, paramiko.AuthenticationException, paramiko.SSHException) as e:
            self.log.error(f"error occurred {e}")
            raise

        self.log.info(f"transferring file {output_file_name}")
        ftp_client = ssh_client.open_sftp()
        ftp_client.put(os.path.join(self.output_path, output_file_name), output_file_name)
        ftp_client.close()
        self.log.info("file transfer done")
        self.log.meta_end_action("upload")

        # influx must be in PATH
        self.log.info(f"importing file on host {Config().ssh_hostname}")
        self.log.meta_create_action("influxdb import")

        import_command = "".join(["influx -import -path=output_", self._type, "_", self._full_delta, ".dat -precision=s"])
        _, stdout, _ = ssh_client.exec_command(import_command)
        self.log.meta_end_action("influxdb import")

        for line in stdout.readlines():
            self.log.info(line.strip())

        ssh_client.close()

        self.cleanup_old_files()

    def run(self):
        """Fetch all data, process it locally, transfer to InfluxDB server and import there."""
        self.fetch_data()
        # self.fetch_station_metadata()
        self.compile_data()
        self.transfer_and_import()
