from abc import ABC

from weather.protocol.csv.WeatherProcessor import WeatherProcessor


class Visibility(WeatherProcessor, ABC):
    """Implement csv data processing for visibility data."""

    def __init__(self):
        """Initiate instance."""
        WeatherProcessor.__init__(self, "visibility")

    def parse_data(self, reader, station, output_file):
        """Read visibilty data from csv observation file."""
        for line in reader:
            if line[0] == "STATIONS_ID":
                continue
            self._lines += 1

            ts = WeatherProcessor.str_to_ts(line[1])
            visibility = WeatherProcessor.fix_data_errors(line[3])

            self.emit_data(source=self._type, output_file=output_file, station=station, component="visibility", value=visibility, epoch=ts)

            self.print_tps()
