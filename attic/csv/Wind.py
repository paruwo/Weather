from abc import ABC

from weather.protocol.csv.WeatherProcessor import WeatherProcessor


class Wind(WeatherProcessor, ABC):
    """Implement csv data processing for wind data."""

    def __init__(self):
        """Initiate instance."""
        WeatherProcessor.__init__(self, "wind")

    def parse_data(self, reader, station, output_file):
        """Read wind data from csv observation file."""
        for line in reader:
            if line[0] == "STATIONS_ID":
                continue
            self._lines += 1

            ts = WeatherProcessor.str_to_ts(line[1].strip())
            speed = WeatherProcessor.fix_data_errors(line[3])
            direction = WeatherProcessor.fix_data_errors(line[4])

            self.emit_data(source=self._type, output_file=output_file, station=station, component="speed", value=speed, epoch=ts)
            self.emit_data(source=self._type, output_file=output_file, station=station, component="direction", value=direction, epoch=ts)

            self.print_tps()
