from abc import ABC

from weather.protocol.csv.WeatherProcessor import WeatherProcessor


class Rain(WeatherProcessor, ABC):
    """Implement csv data processing for rain data."""

    def __init__(self):
        """Initiate instance."""
        WeatherProcessor.__init__(self, "rain")

    def parse_data(self, reader, station, output_file):
        """Read rain data from csv observation file."""
        for line in reader:
            if line[0] == "STATIONS_ID":
                continue

            self._lines += 1

            ts = WeatherProcessor.str_to_ts(line[1])
            duration = WeatherProcessor.fix_data_errors(line[3])
            height = WeatherProcessor.fix_data_errors(line[4])
            rain_flag = WeatherProcessor.fix_data_errors(line[5])

            self.emit_data(source=self._type, output_file=output_file, station=station, component="duration", value=duration, epoch=ts)
            self.emit_data(source=self._type, output_file=output_file, station=station, component="height", value=height, epoch=ts)
            self.emit_data(source=self._type, output_file=output_file, station=station, component="rain_flag", value=rain_flag, epoch=ts)

            self.print_tps()
