from abc import ABC

from weather.protocol.csv.WeatherProcessor import WeatherProcessor


class Solar(WeatherProcessor, ABC):
    """Implement csv data processing for solar emission data."""

    def __init__(self):
        """Initiate instance."""
        WeatherProcessor.__init__(self, "solar")

    def parse_data(self, reader, station, output_file):
        """Read solar data from csv observation file."""
        for line in reader:
            if line[0] == "STATIONS_ID":
                continue
            self._lines += 1

            ts = WeatherProcessor.str_to_ts(line[1])
            sunshine = WeatherProcessor.fix_data_errors(line[5])

            self.emit_data(source=self._type, output_file=output_file, station=station, component="sunshine", value=sunshine, epoch=ts)

            self.print_tps()
