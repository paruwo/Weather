from abc import ABC
from datetime import datetime

from weather.protocol.csv.WeatherProcessor import WeatherProcessor


class Air(WeatherProcessor, ABC):
    """Implement csv data processing for air data."""

    def __init__(self):
        """Initiate instance."""
        WeatherProcessor.__init__(self, "air")

    def parse_data(self, reader, station, output_file):
        """Read air data from csv observation file."""
        start_ts = datetime.now()
        self._lines = 0
        for line in reader:
            if line[0] == "STATIONS_ID":
                continue
            self._lines += 1

            station = line[0]
            ts = WeatherProcessor.str_to_ts(line[1])
            pressure = WeatherProcessor.fix_data_errors(line[3])
            temperature = WeatherProcessor.fix_data_errors(line[4])
            humidity = WeatherProcessor.fix_data_errors(line[6])

            self.emit_data(source=self._type, output_file=output_file, component="pressure", station=station, value=pressure, epoch=ts)
            self.emit_data(
                source=self._type, output_file=output_file, component="temperature", station=station, value=temperature, epoch=ts
            )
            self.emit_data(source=self._type, output_file=output_file, component="humidity", station=station, value=humidity, epoch=ts)

            self.print_tps()

        self.log.info(f"parsing took {datetime.now() - start_ts} seconds")
