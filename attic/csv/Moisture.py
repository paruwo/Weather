from abc import ABC

from weather.protocol.csv.WeatherProcessor import WeatherProcessor


class Moisture(WeatherProcessor, ABC):
    """Implement csv data processing for moisture data."""

    def __init__(self):
        """Initiate instance."""
        WeatherProcessor.__init__(self, "moisture")

    def parse_data(self, reader, station, output_file):
        """Read moisture data from csv observation file."""
        for line in reader:
            if line[0] == "STATIONS_ID":
                continue
            self._lines += 1

            ts = WeatherProcessor.str_to_ts(line[1])
            pressure = WeatherProcessor.fix_data_errors(line[3])
            rel_humidity = WeatherProcessor.fix_data_errors(line[6])

            self.emit_data(source=self._type, output_file=output_file, station=station, component="pressure", value=pressure, epoch=ts)
            self.emit_data(source=self._type, output_file=output_file, station=station, component="humidity", value=rel_humidity, epoch=ts)

            self.print_tps()
