create role weather nosuperuser nocreatedb nocreaterole noinherit login password 'weather';
create role weather_read nosuperuser nocreatedb nocreaterole noinherit login password 'weather_read';
create database weather;
alter database weather owner to weather;
grant usage, create on schema public to weather;
