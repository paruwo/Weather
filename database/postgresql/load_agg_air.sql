CREATE OR REPLACE PROCEDURE load_agg_air(i_past_date TIMESTAMP DEFAULT NULL)
AS $$
DECLARE
    v_past_date TIMESTAMP;
BEGIN
    IF i_past_date IS NULL THEN
       RAISE NOTICE 'empty parameter i_past_date, starting from scratch';
       v_past_date := TIMESTAMP '1900-01-01 00:00:00';

       TRUNCATE TABLE agg_air;
    ELSE
       v_past_date := i_past_date;

       DELETE FROM agg_air WHERE effective_at >= v_past_date;
   END IF;

    INSERT INTO agg_air
    SELECT t.station_id,
           t.effective_at,
           t.pressure,
           l1.pressure AS pressure_1,
           l2.pressure AS pressure_2,
           l5.pressure AS pressure_5,
           t.temperature,
           l1.temperature AS temperature_1,
           l2.temperature AS temperature_2,
           l5.temperature AS temperature_5,
           t.relative_humidity,
           l1.relative_humidity AS relative_humidity_1,
           l2.relative_humidity AS relative_humidity_2,
           l5.relative_humidity AS relative_humidity_5
      FROM air t
      LEFT OUTER JOIN air l1
        ON t.station_id = l1.station_id
       AND t.effective_at - '1 year'::interval = l1.effective_at
      LEFT OUTER JOIN air l2
        ON t.station_id = l2.station_id
       AND t.effective_at - '2 year'::interval = l2.effective_at
      LEFT OUTER JOIN air l5
        ON t.station_id = l5.station_id
       AND t.effective_at - '5 year'::interval = l5.effective_at
     WHERE t.effective_at >= v_past_date
     ORDER BY t.effective_at, t.station_id;
END;
$$ LANGUAGE plpgsql;

commit;
