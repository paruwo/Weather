DROP TABLE IF EXISTS agg_air;

CREATE UNLOGGED TABLE agg_air (
    station_id          SMALLINT NOT NULL,
    effective_at        TIMESTAMP(0) NOT NULL,
    --
    pressure            DECIMAL(5,1),
    pressure_1          DECIMAL(5,1),
    pressure_2          DECIMAL(5,1),
    pressure_5          DECIMAL(5,1),
    --
    temperature         DECIMAL(5,1),
    temperature_1       DECIMAL(5,1),
    temperature_2       DECIMAL(5,1),
    temperature_5       DECIMAL(5,1),
    --
    relative_humidity   SMALLINT,
    relative_humidity_1 SMALLINT,
    relative_humidity_2 SMALLINT,
    relative_humidity_5 SMALLINT
    --
    -- PRIMARY KEY (effective_at, station_id)
);
--CLUSTER agg_air USING agg_air_pkey;

-- Alternatively to PK + clustering a BRIN index is smaller and
-- supports queries comparbly well. Only argument for PK could
-- be additional data quality checks.
CREATE INDEX agg_air_index ON agg_air USING BRIN (effective_at, station_id);

GRANT SELECT ON agg_air TO weather_read;
