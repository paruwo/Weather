DROP TABLE IF EXISTS air CASCADE;

-- support range-based time queries as well as queries on exact stations
-- use version 11 feature "declarative partitioning"
-- subpartitioning not needed
CREATE TABLE air (
    effective_at        TIMESTAMP(0) NOT NULL,
    station_id          SMALLINT NOT NULL,
    ---
    pressure            DECIMAL(5,1),
    temperature         DECIMAL(3,1),
    relative_humidity   SMALLINT,
    ---
    inserted_at         TIMESTAMP(0) NOT NULL DEFAULT now()
)
PARTITION BY RANGE(effective_at)
;

CREATE TABLE air_y1900_2000 PARTITION OF air FOR VALUES FROM ('1900-01-01 00:00:00') TO ('2000-01-01 00:00:00');
CREATE TABLE air_y2000_2005 PARTITION OF air FOR VALUES FROM ('2000-01-01 00:00:00') TO ('2005-01-01 00:00:00');
CREATE TABLE air_y2005_2010 PARTITION OF air FOR VALUES FROM ('2005-01-01 00:00:00') TO ('2010-01-01 00:00:00');
CREATE TABLE air_y2010_2015 PARTITION OF air FOR VALUES FROM ('2010-01-01 00:00:00') TO ('2015-01-01 00:00:00');
CREATE TABLE air_y2015_2020 PARTITION OF air FOR VALUES FROM ('2015-01-01 00:00:00') TO ('2020-01-01 00:00:00');
CREATE TABLE air_y2020_2025 PARTITION OF air FOR VALUES FROM ('2020-01-01 00:00:00') TO ('2025-01-01 00:00:00');
CREATE TABLE air_y2025_2030 PARTITION OF air FOR VALUES FROM ('2025-01-01 00:00:00') TO ('2030-01-01 00:00:00');

ALTER TABLE air ADD CONSTRAINT air_pk PRIMARY key (effective_at, station_id);
-- CLUSTER air USING air_pk;

GRANT SELECT ON air TO weather_read;
