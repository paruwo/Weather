DROP TABLE IF EXISTS rain CASCADE;

-- support range-based time queries as well as queries on exact stations
-- use version 11 feature "declarative partitioning"
-- subpartitioning not needed
CREATE TABLE rain (
    effective_at        TIMESTAMP(0) NOT NULL,
    station_id          SMALLINT NOT NULL,
    ---
    duration            DECIMAL(2,0),
    height              DECIMAL(5,2),
    rain_flag           BOOLEAN NOT NULL,
    ---
    inserted_at         TIMESTAMP(0) NOT NULL DEFAULT now()
)
PARTITION BY RANGE(effective_at)
;

CREATE TABLE rain_y1900_2000 PARTITION OF rain FOR VALUES FROM ('1900-01-01 00:00:00') TO ('2000-01-01 00:00:00');
CREATE TABLE rain_y2000_2005 PARTITION OF rain FOR VALUES FROM ('2000-01-01 00:00:00') TO ('2005-01-01 00:00:00');
CREATE TABLE rain_y2005_2010 PARTITION OF rain FOR VALUES FROM ('2005-01-01 00:00:00') TO ('2010-01-01 00:00:00');
CREATE TABLE rain_y2010_2015 PARTITION OF rain FOR VALUES FROM ('2010-01-01 00:00:00') TO ('2015-01-01 00:00:00');
CREATE TABLE rain_y2015_2020 PARTITION OF rain FOR VALUES FROM ('2015-01-01 00:00:00') TO ('2020-01-01 00:00:00');
CREATE TABLE rain_y2020_2025 PARTITION OF rain FOR VALUES FROM ('2020-01-01 00:00:00') TO ('2025-01-01 00:00:00');
CREATE TABLE rain_y2025_2030 PARTITION OF rain FOR VALUES FROM ('2025-01-01 00:00:00') TO ('2030-01-01 00:00:00');

ALTER TABLE rain ADD CONSTRAINT rain_pk PRIMARY key (effective_at, station_id);
-- CLUSTER rain USING rain_pk;

GRANT SELECT ON rain TO weather_read;
