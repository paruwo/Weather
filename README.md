# Overview

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

This is the weather data import tool. It pulls open weather data
from German Weather Service "dwd" (i.e. Deutscher Wetterdienst) and
ships it to an InfluxDB target. From there it's easy to visualize
data using [grafana](https://grafana.com/).

Feel free to find more information about dwd OpenData [in their homepage](https://www.dwd.de/DE/leistungen/opendata/hilfe.html?nn=16102).

This tool was not made to run in production or any internet-facing
environments. The opposite is true: this is a recreational project
intended to be working in isolated and safe local environments only.

## Features

* get air, solar, rain and wind data from official German Weather Service in 10 minute resolution
* get [MOSMIX forecasts](https://www.dwd.de/DE/leistungen/opendata/neuigkeiten/opendata_mar2018_01.html) for given stations
* ingest into InfluxDB as main Grafana backend
* ingest into PostgreSQL to support weather metrics across years
* collection of metadata in local sqlite data store
* rich configuration options but easy to configure
* run from command line with most common options parametrized

## Installation

### Prerequisits

1. influxdb up and running
2. Optionally setup PostgreSQL database (for aggregated data)
3. Optionally prepare Grafana setup (for visualization)

Please be aware that this tool was tested with IPV4 only but not with IPV6.

```sh
# FreeBSD only
pkg -4 install libxml2 libxslt postgresql13-client py39-sqlite3
```

For Postgresql check and rollout the setup file database/postgres/setup/setup.sql.

```sql
create role weather nosuperuser nocreatedb nocreaterole noinherit login password 'weather';
create role weather_read nosuperuser nocreatedb nocreaterole noinherit login password 'weather_read';
create database weather
alter database weather owner to weather
```

### From code

Please note: only *nix-like backend are supported. Development is done
on GNU/Linux or MacOS with any recent Python. Jenkins CI backend runs on
latest FreeBSD 13 [jails](https://www.freebsd.org/doc/handbook/jails.html)
with single node influxdb 1.x, grafana 8 and PostgreSQL 14.

```sh
git clone https://codeberg.org/paruwo/Weather.git && cd Weather
pyenv virtualenv 3.13 weather
pyenv local weather
# .. terminal switches to venv
python3 -m pip install poetry==1.8.3

# install dependencies and weather app
poetry install --only main

# for development or any other special purpose, feel free to install any extras
poetry install --extras "quality test postgres"
```

### Run it

Now, we are ready to run the app. Call Weather, e.g. to process air
and solar initially (full) and then also for the previous day up
to now (delta) both using http protocol:

```sh
# single initial load of most important weather data
python3 Weather.py -t air rain solar wind cloud -m http --full

# regular delta load into both DBs
python3 Weather.py -m http --delta -t air rain solar wind cloud --target influxdb
python3 Weather.py -m http --delta -t air --target postgresql

# load historical data to postgresql for cross-year analysis
python3 Weather.py -t air rain -m http --hist --target postgresql
```

### Docker

Docker images are neither tested not broadly in use by me - so be warned.
However, building the container is straight forward:

```sh
docker build -t paruwo/weather .

# for interactive debugging use this (mind switch "-t")
# docker run -it --rm --name weather -d paruwo/weather
```

Now we can run a data refresh, for instance to fetch solar data for past 365:

```sh
docker run -it --rm --name weather \
    -e FULL_DELTA_MODE=full \
    -e WEATHER_TYPES=solar \
    -d paruwo/weather
```

Please mind that only the basic functionality is currently included but
no extra modules. Docker is also not included of the CI pipeline.

To upload to a private registry:

```sh
# docker tag [OPTIONS] IMAGE[:TAG] [REGISTRYHOST/][USERNAME/]NAME[:TAG]
docker tag paruwo/weather portainer.fritz.box:49153/weather
docker push portainer.fritz.box:49153/weather

```

## Parameter

```sh
usage: Weather.py [-h] [-v]
                [-t {rain,air,wind,solar,cloud,visibility} [{rain,air,wind,solar,cloud,visibility} ...]]
                [-m {http}] [-f] [--delta] [--hist] [--auto]
                [--target {influxdb,postgresql}] [--validate-config]
                [--benchmark {local,postgresql,influxdb}]
                [-i influx_endpoint] [-d DATABASE] [-b HTTP_BATCH_SIZE]

Download and import weather data into influx db.

    optional arguments:
    -h, --help            show this help message and exit
    -v, --version         show program's version number and exit
    -t {rain,air,wind,solar,cloud,visibility} [{rain,air,wind,solar,cloud,visibility} ...], --type {rain,air,wind,solar,cloud,visibility} [{rain,air,wind,solar,cloud,visibility} ...]
                            weather type to import
    -m {http}, --method {http}
                            Transfer method to use, only HTTP
    -f, --full            Load data for last 500 days, standard is delta.
    --delta               Load data for last and current day, this is standard.
    --hist                Load data for all existing history.
    --auto                Determine which data ranges are missing and load it
                            automatically.
    --target {influxdb,postgresql}
                            Select target database type to write data into.
    --validate-config     Validate config by tying to connect to all configured
                            connections.
    --benchmark {local,postgresql,influxdb}
                            Run tiny benchmark against selected system.
    -i influx_endpoint, --influx influx_endpoint
                            Complete REST endpoint for influxdb server, e.g.
                            http://10.0.1.3:8086
    -d DATABASE, --database DATABASE
                            Influxdb database name, e.g. weather
    -b HTTP_BATCH_SIZE, --http-batch-size HTTP_BATCH_SIZE
                            Gather so many records and send them in one batch,
                            should be less than 5000 according to influxdb
                            documentation
```

## Extras

### Parallel Processing

Attention: Here be dragons.

The files to process are quite small, up to some MB raw data only.
The work behind can be easily achieved even by old or energy saving CPUs in couple of seconds.
So it makes little sense to apply parallel processing on single-file level.

Also the target systems (influxdb, postgres) need be able to cope with the
amount of data arriving. So even if we could parallelize everything dramatically,
this could only work having a proper data receiver in place.

In general, the parallel approach works best on high level where non-trivial
tasks can be split into isolated work packages.
Thus, parallel processing is applicable only in the following case:

1. process more than one weather type

### Metadata (in progress)

Installation:

```sh
cd Weather
poetry install --extras "metadata"
```

### Web Services (not much to see yet)

Additionally to the core functionality there are [web services](http://127.0.0.1:5000)
available to:

* inform about previous runs
* show basic administration
* allow machine interaction using REST-ful interfaces
* (future work: monitoring)

Installation:

```sh
cd Weather

# install plugin
poetry install --extras "postgres"

# then run it
python3 weather/webservice/web_service.py
```

After having started, check out the [index page](http://127.0.0.1:5000)
for more information and further actions.

## Dev Cheat Sheet

```sh
# install dev tools
poetry install --extras "quality test benchmark"

#
cd weather
pre-commit run -a

# execute unit tests
python3 -m pytest
# execute unit tests and collect code coverage stats
coverage run -m pytest && coverage html && firefox htmlcov/index.html

# run local benchmark (single core performance matters only)
python3 Weather.py --benchmark local

# trace app
python3 -m cProfile -o trace.prof Weather.py -t air -m http --full
tuna trace.prof

# security check
safety check --bare

# in Jenkins add the following plugins, document it
# Pyenv Pipeline Plugin
# Cobertura
# Warnings Next Generation

# check how to install Docker on Manjaro: https://manjaro.site/how-to-install-docker-on-manjaro-18-0/
```

## TODO

1) net: check IPv6 setup (adapt inet/pf routing setup at home)
2) tune: run app trace and fix top 3 performance issues (cProfile + tuna)
3) efficiency: go for [RFC3339 timestamp, see](https://awesome.influxdata.com/docs/part-2/influxdb-data-model/#assumptions-and-conventions)
4) efficiency: concurrent async data upload to influxdb & PostgreSQL (hard)
5) efficiency: check pandas / numpy
6) performance: check numba
