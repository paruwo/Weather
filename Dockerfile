# https://hub.docker.com/_/python/?tab=description
# https://stackoverflow.com/questions/53835198/integrating-python-poetry-with-docker

FROM python:3.11-slim-bookworm

ENV FULL_DELTA_MODE=delta \
    WEATHER_TYPES='rain air wind solar cloud' \
    TARGET_SYSTEM=influxdb \
    INFLUX_TARGET=https://server.fritz.box:8086 \
    INFLUX_DB=weather \
    # python
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    # poetry
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_CACHE_DIR='/var/cache/pypoetry' \
    POETRY_HOME='/usr/local'

# install poetry and clean
# https://github.com/python-poetry/poetry
SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
RUN apt-get update && apt-get -y upgrade && \
    apt-get install --no-install-recommends -y curl && \
    # install poetry
    curl -sSL 'https://install.python-poetry.org' | python - && \
    poetry --version && \
    # cleanup
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*

# copy essential app project files
WORKDIR /app
COPY pyproject.toml poetry.lock /app/

# hard installations
RUN mkdir -p /app/data/{rain,air,wind,solar,cloud,visibility,forecast} && \
    # poetry run pip install -U pip && \
    poetry install --no-interaction --no-ansi --no-dev && \
    rm -rf "$POETRY_CACHE_DIR"

# app code
COPY weather/ /app/weather/
COPY Weather.py /app/

# run
CMD ["sh", "-c", \
     "python -u Weather.py -m http --$FULL_DELTA_MODE -t $WEATHER_TYPES --target $TARGET_SYSTEM --influx $INFLUX_TARGET -d $INFLUX_DB"]
