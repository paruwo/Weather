import logging
import os
import platform
import sys
from argparse import ArgumentParser, Namespace
from multiprocessing import Pool

from weather.benchmark.local import Local
from weather.protocol.http import solar
from weather.protocol.http.visibility import Visibility
from weather.util.config import Config
from weather.util.logger import Logger


class Weather:
    """Main project class to be called."""

    args: Namespace
    log: logging.Logger
    parser: ArgumentParser

    def print_info(self):
        """Print some information about this program and the computer it is running on."""
        self.log.info(f"This is weather v{Config().version}.")
        self.log.info(f"Working directory is {os.getcwd()}")
        self.log.info(f"Architecture: {platform.machine()} {platform.architecture()[0]} with {os.cpu_count()} threads")

    def __init__(self):
        """Initialize instance."""
        self.log = Logger()

    def parse_arguments(self):
        """Command line option parser."""
        self.log.debug("parsing arguments")
        self.parser = ArgumentParser(description="Download and import weather data into influx db.")
        self.parser.add_argument("-v", "--version", action="version", version="%(prog)s " + Config().version)
        self.parser.add_argument(
            "-t",
            "--type",
            dest="weather_type",
            choices=Config().weather_types,  # +'air-map'
            help="weather type to import",
            nargs="+",
        )
        self.parser.add_argument(
            "-m", "--method", dest="method", choices=["http"], default="http", help="Transfer method to use, only HTTP"
        )
        self.parser.add_argument(
            "-f", "--full", dest="full", default=False, action="store_true", help="Load data for last 500 days, standard is delta."
        )
        self.parser.add_argument(
            "--delta", dest="full", default=True, action="store_false", help="Load data for last and current day, this is standard."
        )
        self.parser.add_argument("--hist", dest="hist", default=False, action="store_true", help="Load data for all existing history.")
        self.parser.add_argument(
            "--auto",
            dest="auto",
            default=False,
            action="store_true",
            help="Determine which data ranges are missing and load it automatically.",
        )
        self.parser.add_argument(
            "--target",
            dest="target",
            default="influxdb",
            choices=["influxdb", "postgresql"],
            help="Select target database type to write data into.",
        )

        self.parser.add_argument(
            "--validate-config",
            dest="validate_config",
            default=False,
            action="store_true",
            help="Validate config by tying to connect to all configured connections.",
        )

        self.parser.add_argument(
            "--benchmark", dest="benchmark", choices=["local", "postgresql", "influxdb"], help="Run tiny benchmark against selected system."
        )

        self.parser.add_argument(
            "-i",
            "--influx",
            dest="influx",
            metavar="influx_endpoint",
            default=Config().influxdb_endpoint,
            help="Complete REST endpoint for influxdb server, e.g. http://10.0.1.3:8086",
        )
        self.parser.add_argument(
            "-d", "--database", dest="database", default=Config().influxdb_database_name, help="Influxdb database name, e.g. weather"
        )
        self.parser.add_argument(
            "-b",
            "--http-batch-size",
            dest="http_batch_size",
            help="Gather so many records and send them in one batch, should be less than 5000 \
                                  according to influxdb documentation",
        )
        # self.parser.add_argument('-c', '--http-compression', dest='http_compression', action='store_true',
        #                    help='If client and server support http messages are compressed,')

        self.args = self.parser.parse_args()

    def interpret_arguments(self):
        """Evaluate given program options."""
        self.log.debug("evaluating parsed arguments")
        self.log.debug(self.args)

        if not self.args.weather_type and not self.args.benchmark:
            self.log.error("no weather type(s) or benchmark mode selected")
            self.parser.print_help()
            sys.exit(0)

        if self.args.method != "http":
            self.log.error(f"unsupported transportation method {self.args.method}")
            self.parser.print_help()
            sys.exit(0)
        else:
            # http
            if not self.args.influx or not self.args.database:
                self.log.error("for HTTP mode the parameters influx and database are mandatory")
                self.parser.print_help()
                sys.exit(0)

            Config().influxdb_endpoint = self.args.influx
            Config().influxdb_database_name = self.args.database

    def influxdb_http_serial(self, weather_types: list, full_delta_type: str):
        """
        Process weather observations for InfluxDB for the given weather types in a serial manner.

        :param weather_types: list of weather types to process
        :param full_delta_type: full or delta
        """
        # pylint: disable=import-outside-toplevel
        from weather.protocol.http.air import Air
        from weather.protocol.http.air_map import AirMap
        from weather.protocol.http.cloud import Cloud
        from weather.protocol.http.rain import Rain
        from weather.protocol.http.solar import Solar
        from weather.protocol.http.wind import Wind

        if len(weather_types) > 1:
            self.log.info(f"called {len(weather_types)} weather types to process")

        weather_classes = {
            "air": Air,
            "rain": Rain,
            "wind": Wind,
            "solar": Solar,
            "air-map": AirMap,
            "cloud": Cloud,
            "visibility": Visibility,
        }

        for weather_type in weather_types:
            # create and instance of map class
            processor = weather_classes[weather_type]()

            # can be full+delta according to "auto" mode
            data_ranges = []
            if full_delta_type == "auto":
                data_ranges = processor.determine_data_range()
            else:
                # single value
                data_ranges.append(full_delta_type)

            for data_range in data_ranges:
                if full_delta_type not in ["full", "hist", "delta"]:
                    raise RuntimeError(f"unknown data range {full_delta_type}")
                elif data_range == "full":
                    processor.make_full().run()
                elif data_range == "hist":
                    processor.make_hist().run()
                elif data_range == "delta":
                    processor.make_delta().run()

    def postgresql_serial(self, weather_types: list, full_delta_type: str):
        """
        Process weather observations for PostgreSQL for the given weather types in a serial manner.

        :param weather_types: list of weather types to process
        :param full_delta_type: full or delta
        """
        # pylint: disable=import-outside-toplevel
        from weather.protocol.database.air import Air
        from weather.protocol.database.rain import Rain

        for weather_type in weather_types:
            if weather_type == "air":
                processor = Air()
            elif weather_type == "rain":
                processor = Rain()
            else:
                self.log.error(f"type {weather_type} not implemented for PostgreSQL")
                return

            if full_delta_type not in ["full", "hist", "delta"]:
                raise RuntimeError(f"unknown data range {full_delta_type}")
            elif full_delta_type == "full":
                processor.make_full().run()
            elif full_delta_type == "hist":
                processor.make_hist().run()
            elif full_delta_type == "delta":
                processor.make_delta().run()

    @staticmethod
    def http_delta_parallel(weather_types: list):
        """
        Attention, this might eat up you cat or drink its milk - early state and untested.

        Processes weather observations for the given weather types, but parallel by using
        pool-based tasks. Cannot be called from command line.
        :param weather_types: list of weather types to process
        """
        # pylint: disable=import-outside-toplevel
        from weather.protocol.http.air import Air
        from weather.protocol.http.rain import Rain
        from weather.protocol.http.solar import Solar
        from weather.protocol.http.wind import Wind

        deadline_seconds = 30

        num_cpu = min(os.cpu_count(), len(Config().weather_types), len(weather_types))
        Logger().info(f"will spawn {num_cpu} process(es) in parallel")

        processes = []

        with Pool(num_cpu) as pool:
            for weather_type in weather_types:
                if weather_type == "air":
                    processes.append(Air().make_delta())
                elif weather_type == "rain":
                    processes.append(Rain().make_delta())
                elif weather_type == "wind":
                    processes.append(Wind().make_delta())
                elif weather_type == "solar":
                    processes.append(Solar().make_delta())

            process_results = [pool.apply_async(t.run, ()) for t in processes]

            Logger().info("starting processing... (deadline is %i)" % deadline_seconds)
            for res in process_results:
                res.get(timeout=deadline_seconds)

    def get_full_delta_type(self) -> str:
        """Determine loading type from input parameters."""
        if self.args.full:
            return "full"
        if self.args.hist:
            return "hist"
        if self.args.auto:
            return "auto"
        return "delta"


if __name__ == "__main__":
    weather = Weather()

    weather.parse_arguments()
    weather.interpret_arguments()

    if weather.args.benchmark == "local":
        Local.run()
        sys.exit(0)
    elif weather.args.benchmark == "postgresql":
        from weather.benchmark.postgresql import Postgresql

        with Postgresql() as pg_bench:
            pg_bench.run()
        sys.exit(0)
    elif weather.args.benchmark == "influxdb":
        from weather.benchmark.influxdb import InfluxDb

        InfluxDb().run()
        sys.exit(0)

    if weather.args.validate_config:
        Logger().info("running validation check against influxdb")
        influx = solar.Solar().check_influxdb()
        Logger().info("influx connection ok :)")

        Logger().info("running validation check against database")
        from weather.protocol.database.air import Air

        postgres = Air()
        postgres.validate_pg_version()
        Logger().info("database connection ok :)")

        sys.exit(0)

    weather.print_info()

    FULL_DELTA_TYPE = weather.get_full_delta_type()

    if weather.args.target not in ("influxdb", "postgresql"):
        raise RuntimeWarning("cannot determine target to write data to")
    elif weather.args.target == "influxdb":
        if weather.args.method != "http":
            raise RuntimeWarning(f"calling method {weather.args.method} for {weather.args.full} load makes no sense")
        else:
            weather.influxdb_http_serial(weather_types=weather.args.weather_type, full_delta_type=FULL_DELTA_TYPE)
    elif weather.args.target == "postgresql":
        weather.postgresql_serial(weather_types=weather.args.weather_type, full_delta_type=FULL_DELTA_TYPE)
